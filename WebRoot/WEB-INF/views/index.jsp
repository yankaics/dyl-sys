<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>dyl-sys后台权限管理系统</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">
		<%@include file="common/commonCss.jsp"%>
		<link rel="stylesheet" href="${res}/css/global.css" media="all">
		<link rel="stylesheet" href="${res}/plugins/font-awesome/css/font-awesome.min.css">
	</head>
	<body>
		<div class="layui-layout layui-layout-admin" style="border-bottom: solid 5px #1aa094;">
			<div class="layui-header header header-demo">
				<div class="layui-main">
					<div class="admin-login-box">
						<a class="logo" style="left: 0;" href="####">
							<span style="font-size: 22px;">后台管理系统</span>
						</a>
						<div class="admin-side-toggle">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</div>
						<div class="admin-side-full">
							<i class="fa fa-life-bouy" aria-hidden="true"></i>
						</div>
					</div>
					<ul class="layui-nav admin-header-item">
						<!-- <li class="layui-nav-item">
							<a href="javascript:;">清除缓存</a>
						</li>
						<li class="layui-nav-item">
							<a href="javascript:;">浏览网站</a>
						</li>
						<li class="layui-nav-item" id="video1">
							<a href="javascript:;">视频</a>
						</li> -->
						<li class="layui-nav-item">
							<a href="javascript:;" class="admin-header-user">
								<%-- <img src="${res}/images/0.jpg" /> --%>
								<span>${SYS_USER.username}</span>
							</a>
							<dl class="layui-nav-child">
								<!-- <dd>
									<a href="javascript:;"><i class="fa fa-user-circle" aria-hidden="true"></i> 个人信息</a>
								</dd>
								<dd>
									<a href="javascript:;"><i class="fa fa-gear" aria-hidden="true"></i> 设置</a>
								</dd>
								<dd id="lock">
									<a href="javascript:;">
										<i class="fa fa-lock" aria-hidden="true" style="padding-right: 3px;padding-left: 1px;"></i> 锁屏 (Alt+L)
									</a>
								</dd> -->
								<dd>
									<a href="####" id="modifyPwd"><i class="fa fa-gear" aria-hidden="true"></i>修改密码</a>
								</dd>
								<dd>
									<a href="logout.do"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
								</dd>
							</dl>
						</li>
					</ul>
					<ul class="layui-nav admin-header-item-mobile">
						<li class="layui-nav-item">
							<a href="logout.do"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="layui-side layui-bg-black" id="admin-side">
				<div class="layui-side-scroll" id="admin-navbar-side" lay-filter="side"></div>
			</div>
			<div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;" id="admin-body">
				<div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="admin-tab">
					<ul class="layui-tab-title">
						<li class="layui-this">
							<i class="fa fa-dashboard" aria-hidden="true"></i>
							<cite>控制面板</cite>
						</li>
					</ul>
					<div class="layui-tab-content" style="min-height: 150px; padding: 5px 0 0 0;">
						<div class="layui-tab-item layui-show">
							<iframe src="main.do"></iframe>
						</div>
					</div>
				</div>
			</div>
			<div class="layui-footer footer footer-demo" id="admin-footer">
				<div class="layui-main">
					<p>2016 &copy;
						<!-- <a href="####">m.zhengjinfan.cn/</a> --> LGPL license
					</p>
				</div>
			</div>
			<div class="site-tree-mobile layui-hide">
				<i class="layui-icon">&#xe602;</i>
			</div>
			<div class="site-mobile-shade"></div>
			
			<!--锁屏模板 start-->
			<script type="text/template" id="lock-temp">
				<div class="admin-header-lock" id="lock-box">
					<div class="admin-header-lock-img">
						<img src="${res}/images/0.jpg"/>
					</div>
					<div class="admin-header-lock-name" id="lockUserName">beginner</div>
					<input type="text" class="admin-header-lock-input" value="输入密码解锁.." name="lockPwd" id="lockPwd" />
					<button class="layui-btn layui-btn-small" id="unlock">解锁</button>
				</div>
			</script>
			<!--锁屏模板 end -->
			<!-- 修改密码 -->
			<script type="text/template" id="modifyPwdForm">
				<form class="layui-form" action=""  ><!--编辑表单-->
				<div class="layui-form-item" >
					<label class="layui-form-label" style="width: 120px;">原密码</label>
					<div class="layui-input-inline">
					  <input type="text" name="oldPassword" required  lay-verify="required" placeholder="请输入原密码" value="" class="layui-input"  />
					</div>
			    </div>
			    <div class="layui-form-item" >
					<label class="layui-form-label" style="width: 120px;">新密码</label>
					<div class="layui-input-inline">
					  <input type="password" name="password" required  lay-verify="required" placeholder="请输入新密码" value="" class="layui-input"  />
					</div>
			    </div>
				<div class="layui-form-item" >
					<label class="layui-form-label" style="width: 120px;">再次输入新密码</label>
					<div class="layui-input-inline">
					  <input type="password" name="passwordAgain" required  lay-verify="required" placeholder="再次输入新密码" value="" class="layui-input"  />
					</div>
			    </div>
				</form>
			</script>
			<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
			<script src="${res}/js/index.js"></script>
			<script>
				layui.use('layer', function() {
					var $ = layui.jquery,
						layer = layui.layer;

					$('#video1').on('click', function() {
						layer.open({
							title: 'YouTube',
							maxmin: true,
							type: 2,
							content: 'video.html',
							area: ['800px', '500px']
						});
					});

				});
				var pwdDiv = $('#modifyPwdForm').html();
				$('#modifyPwd').click(function(){
					layer.open({
						title: '修改密码',
						maxmin: true,
						type: 1,
						btn: ['保存', '取消'],
						content: pwdDiv,
						area: ['500px', '300px'],
						shade:0.5,//遮罩透明度
						yes:function(index){//点击保存按钮
							if($('[name="oldPassword"]').val()==""){
								layer.msg("原密码不能为空!");
								return;
							}
							if($('[name="password"]').val()==""){
								layer.msg("新密码不能为空!");
								return;
							}
							if($('[name="password"]').val()!=$('[name="passwordAgain"]').val()){
								layer.msg("两次密码不一致!");
								return;
							}
							getHtmlDataByPost("modifyPwd.do","oldPassword="+$('[name="oldPassword"]').val()+"&password="+$('[name="password"]').val(),function(data){
								if(data!=""){
									layer.msg(data);
									return;
								}else{
									layer.msg("密码修改成功!");
									return;
								}
							},true);
						}
					});
				});
			</script>
		</div>
	</body>
</html>