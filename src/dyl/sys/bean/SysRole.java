/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-04-05 15:42:37
*/
package dyl.sys.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysRole implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：主键
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：角色名称
	*对应db字段名:name 类型:VARCHAR2(200)  
	*是否可以为空:是
	*/
	
	private  String  name;
	
	/*字段说明：备注
	*对应db字段名:note 类型:VARCHAR2(200)  
	*是否可以为空:是
	*/
	
	private  String  note;
	
	/*字段说明：创建者Id
	*对应db字段名:creator 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  creator;
	
	/*字段说明：创建时间
	*对应db字段名:create_time 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public BigDecimal getCreator() {
        return creator;
    }
    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"name:"+name+"\n"+
		
		"note:"+note+"\n"+
		
		"creator:"+creator+"\n"+
		
		"createTime:"+createTime+"\n"+
		"";
	}
}
