/*
Navicat MySQL Data Transfer

Source Server         : dyl
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : dyl-sys

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-06-22 16:13:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_role`;
CREATE TABLE `sys_auth_role` (
  `id` decimal(10,0) NOT NULL COMMENT '角色id',
  `role_id` decimal(10,0) DEFAULT NULL COMMENT ' 角色id',
  `menu_id` decimal(10,0) DEFAULT NULL COMMENT '菜单id',
  `kind_id` decimal(10,0) DEFAULT NULL COMMENT '权限id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_auth_role
-- ----------------------------
INSERT INTO `sys_auth_role` VALUES ('110220', '2', '2', '4', '2017-06-16 16:42:33');
INSERT INTO `sys_auth_role` VALUES ('110219', '2', '2', '2', '2017-06-16 16:42:33');
INSERT INTO `sys_auth_role` VALUES ('110218', '2', '2', '1', '2017-06-16 16:42:33');

-- ----------------------------
-- Table structure for sys_fileinfo
-- ----------------------------
DROP TABLE IF EXISTS `sys_fileinfo`;
CREATE TABLE `sys_fileinfo` (
  `id` int(11) DEFAULT NULL,
  `real_name` varchar(100) DEFAULT NULL COMMENT '文件存储的真实名称',
  `upload_name` varchar(100) DEFAULT NULL COMMENT '上传文件名称',
  `file_size` decimal(10,0) DEFAULT NULL COMMENT '文件大小',
  `file_url` varchar(200) DEFAULT NULL COMMENT '文件下载地址',
  `creator` decimal(10,0) DEFAULT NULL COMMENT '上传人',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上传时间',
  `c_id` int(11) DEFAULT NULL COMMENT '关联的业务id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_fileinfo
-- ----------------------------
INSERT INTO `sys_fileinfo` VALUES ('110312', '201706221607951.docx', 'zhuanhuan.docx', '44889', 'download.do?fileName=201706221607951.docx&name=zhuanhuan.docx', '0', null, null);
INSERT INTO `sys_fileinfo` VALUES ('110313', '201706221607464.doc', 'zhuanhuan.doc', '12128', 'download.do?fileName=201706221607464.doc&name=zhuanhuan.doc', '0', null, null);
INSERT INTO `sys_fileinfo` VALUES ('110320', '201706221609549.docx', 'zhuanhuan.docx', '44889', 'download.do?fileName=201706221609549.docx&name=zhuanhuan.docx', '0', '2017-06-22 16:09:48', null);

-- ----------------------------
-- Table structure for sys_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_kind`;
CREATE TABLE `sys_kind` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(20) DEFAULT NULL COMMENT '种类名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_kind
-- ----------------------------
INSERT INTO `sys_kind` VALUES ('1', '查看');
INSERT INTO `sys_kind` VALUES ('2', '新增');
INSERT INTO `sys_kind` VALUES ('3', '修改');
INSERT INTO `sys_kind` VALUES ('4', '删除');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `para` varchar(255) DEFAULT NULL,
  `loginUser` bigint(20) DEFAULT NULL COMMENT '登陆用户',
  `ip` varchar(255) DEFAULT NULL COMMENT 'Ip地址',
  `time` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('109114', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 10:28:34');
INSERT INTO `sys_log` VALUES ('109115', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-05-26 10:28:45');
INSERT INTO `sys_log` VALUES ('109116', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:46');
INSERT INTO `sys_log` VALUES ('109117', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:46');
INSERT INTO `sys_log` VALUES ('109118', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:46');
INSERT INTO `sys_log` VALUES ('109119', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:53');
INSERT INTO `sys_log` VALUES ('109120', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:53');
INSERT INTO `sys_log` VALUES ('109121', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:54');
INSERT INTO `sys_log` VALUES ('109122', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:55');
INSERT INTO `sys_log` VALUES ('109123', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:59');
INSERT INTO `sys_log` VALUES ('109124', '/sysRole!auth.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:29:00');
INSERT INTO `sys_log` VALUES ('109125', '/sysMenu!getMenuForZtree.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:29:00');
INSERT INTO `sys_log` VALUES ('109126', '/sysRole!auth.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:24');
INSERT INTO `sys_log` VALUES ('109127', '/sysMenu!getMenuForZtree.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:24');
INSERT INTO `sys_log` VALUES ('109128', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:28');
INSERT INTO `sys_log` VALUES ('109129', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:28');
INSERT INTO `sys_log` VALUES ('109130', '/sysRole!saveAuth.do', '?authRoleKinds=3_2,4_1,4_2,4_3,4_4&roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:34');
INSERT INTO `sys_log` VALUES ('109131', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:35');
INSERT INTO `sys_log` VALUES ('109132', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:35');
INSERT INTO `sys_log` VALUES ('109133', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:49');
INSERT INTO `sys_log` VALUES ('109134', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:49');
INSERT INTO `sys_log` VALUES ('109135', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:31:22');
INSERT INTO `sys_log` VALUES ('109136', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:31:22');
INSERT INTO `sys_log` VALUES ('109137', '/sysRole!saveAuth.do', '?authRoleKinds=4_1,4_2,4_3,4_4&roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:31:24');
INSERT INTO `sys_log` VALUES ('109138', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:37');
INSERT INTO `sys_log` VALUES ('109139', '/inde.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:44');
INSERT INTO `sys_log` VALUES ('109140', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:44');
INSERT INTO `sys_log` VALUES ('109141', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109142', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109143', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109144', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109145', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:49');
INSERT INTO `sys_log` VALUES ('109146', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:49');
INSERT INTO `sys_log` VALUES ('109147', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:49');
INSERT INTO `sys_log` VALUES ('109148', '/sysRole!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:51');
INSERT INTO `sys_log` VALUES ('109149', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109150', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109151', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109152', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109153', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:00');
INSERT INTO `sys_log` VALUES ('109154', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:22');
INSERT INTO `sys_log` VALUES ('109155', '/sysRole!sysRoleForm.do', '?id=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:28');
INSERT INTO `sys_log` VALUES ('109156', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:30');
INSERT INTO `sys_log` VALUES ('109157', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:30');
INSERT INTO `sys_log` VALUES ('109158', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_3&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:35');
INSERT INTO `sys_log` VALUES ('109159', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:20');
INSERT INTO `sys_log` VALUES ('109160', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109161', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109162', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109163', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109164', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:23');
INSERT INTO `sys_log` VALUES ('109165', '/sysRole!sysRoleForm.do', '?id=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:33');
INSERT INTO `sys_log` VALUES ('109166', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:35');
INSERT INTO `sys_log` VALUES ('109167', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:35');
INSERT INTO `sys_log` VALUES ('109168', '/sysRole!saveAuth.do', '?authRoleKinds=2_4,4_1,4_2,4_3,4_4&roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:38');
INSERT INTO `sys_log` VALUES ('109169', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:43');
INSERT INTO `sys_log` VALUES ('109170', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:46');
INSERT INTO `sys_log` VALUES ('109171', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109172', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109173', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109174', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109175', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:50');
INSERT INTO `sys_log` VALUES ('109176', '/sysRole!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:51');
INSERT INTO `sys_log` VALUES ('109177', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:19:57');
INSERT INTO `sys_log` VALUES ('109178', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:19:57');
INSERT INTO `sys_log` VALUES ('109179', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109180', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109181', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109182', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109183', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:00');
INSERT INTO `sys_log` VALUES ('109184', '/sysRole!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:02');
INSERT INTO `sys_log` VALUES ('109185', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:09');
INSERT INTO `sys_log` VALUES ('109186', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109187', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109188', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109189', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109190', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:12');
INSERT INTO `sys_log` VALUES ('109191', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:13');
INSERT INTO `sys_log` VALUES ('109192', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:15');
INSERT INTO `sys_log` VALUES ('109193', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:16');
INSERT INTO `sys_log` VALUES ('109194', '/sysQuartz!update.do', '?id=1&cronexpression=0/3 * * * * ?&methodname=haha&concurrent=1&state=0&jobdetailname=detailname&targetobject=com.sys.quartz.TestQuarz&triggername=测试1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:21');
INSERT INTO `sys_log` VALUES ('109195', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:22');
INSERT INTO `sys_log` VALUES ('109196', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:21:19');
INSERT INTO `sys_log` VALUES ('109197', '/logout.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:01');
INSERT INTO `sys_log` VALUES ('109198', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109199', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109200', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109201', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109202', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:06');
INSERT INTO `sys_log` VALUES ('109203', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:26:52');
INSERT INTO `sys_log` VALUES ('109204', '/sysRole!main.do', '?name=&currentPage=1&totalPage=1', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:03');
INSERT INTO `sys_log` VALUES ('109205', '/sysRole!main.do', '?name=&currentPage=1&totalPage=1', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:35');
INSERT INTO `sys_log` VALUES ('109206', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:36');
INSERT INTO `sys_log` VALUES ('109207', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:36');
INSERT INTO `sys_log` VALUES ('109208', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:38');
INSERT INTO `sys_log` VALUES ('109209', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:38');
INSERT INTO `sys_log` VALUES ('109210', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:41');
INSERT INTO `sys_log` VALUES ('109211', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:41');
INSERT INTO `sys_log` VALUES ('109212', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:45');
INSERT INTO `sys_log` VALUES ('109213', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:45');
INSERT INTO `sys_log` VALUES ('109214', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:35:12');
INSERT INTO `sys_log` VALUES ('109215', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:35:12');
INSERT INTO `sys_log` VALUES ('109216', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:13');
INSERT INTO `sys_log` VALUES ('109217', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:13');
INSERT INTO `sys_log` VALUES ('109218', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:20');
INSERT INTO `sys_log` VALUES ('109219', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:20');
INSERT INTO `sys_log` VALUES ('109220', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:49');
INSERT INTO `sys_log` VALUES ('109221', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:49');
INSERT INTO `sys_log` VALUES ('109222', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:51');
INSERT INTO `sys_log` VALUES ('109223', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:51');
INSERT INTO `sys_log` VALUES ('109224', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_3&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:53');
INSERT INTO `sys_log` VALUES ('109225', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:55:01');
INSERT INTO `sys_log` VALUES ('109226', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:55:01');
INSERT INTO `sys_log` VALUES ('109227', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_1,2_3,2_3&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:55:02');
INSERT INTO `sys_log` VALUES ('109228', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 13:55:34');
INSERT INTO `sys_log` VALUES ('109229', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 13:55:34');
INSERT INTO `sys_log` VALUES ('109230', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109231', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109232', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109233', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109234', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:51');
INSERT INTO `sys_log` VALUES ('109235', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:53');
INSERT INTO `sys_log` VALUES ('109236', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:53');
INSERT INTO `sys_log` VALUES ('109237', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:55');
INSERT INTO `sys_log` VALUES ('109238', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:55');
INSERT INTO `sys_log` VALUES ('109239', '/sysRole!saveAuth.do', '?authRoleKinds=2_3,2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:57');
INSERT INTO `sys_log` VALUES ('109240', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:58');
INSERT INTO `sys_log` VALUES ('109241', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:58');
INSERT INTO `sys_log` VALUES ('109242', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:09');
INSERT INTO `sys_log` VALUES ('109243', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:09');
INSERT INTO `sys_log` VALUES ('109244', '/sysRole!saveAuth.do', '?authRoleKinds=2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:12');
INSERT INTO `sys_log` VALUES ('109245', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:36');
INSERT INTO `sys_log` VALUES ('109246', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:36');
INSERT INTO `sys_log` VALUES ('109247', '/sysRole!saveAuth.do', '?authRoleKinds=2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:39');
INSERT INTO `sys_log` VALUES ('109248', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:58');
INSERT INTO `sys_log` VALUES ('109249', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:58');
INSERT INTO `sys_log` VALUES ('109250', '/sysRole!saveAuth.do', '?authRoleKinds=2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:01:00');
INSERT INTO `sys_log` VALUES ('109251', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:15');
INSERT INTO `sys_log` VALUES ('109252', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:15');
INSERT INTO `sys_log` VALUES ('109253', '/sysRole!saveAuth.do', '?authRoleKinds=2_4,3_1&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:18');
INSERT INTO `sys_log` VALUES ('109254', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:19');
INSERT INTO `sys_log` VALUES ('109255', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:19');
INSERT INTO `sys_log` VALUES ('109256', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_4,3_1&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:22');
INSERT INTO `sys_log` VALUES ('109257', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:23');
INSERT INTO `sys_log` VALUES ('109258', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:23');
INSERT INTO `sys_log` VALUES ('109259', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:53');
INSERT INTO `sys_log` VALUES ('109260', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:53');
INSERT INTO `sys_log` VALUES ('109261', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:53');
INSERT INTO `sys_log` VALUES ('109262', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:54');
INSERT INTO `sys_log` VALUES ('109263', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:55');
INSERT INTO `sys_log` VALUES ('109264', '/sysQuartz!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:56');
INSERT INTO `sys_log` VALUES ('109265', '/logout.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:46');
INSERT INTO `sys_log` VALUES ('109266', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109267', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109268', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109269', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109270', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:51');
INSERT INTO `sys_log` VALUES ('109271', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:51');
INSERT INTO `sys_log` VALUES ('109272', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:51');
INSERT INTO `sys_log` VALUES ('109273', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109274', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109275', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109276', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109277', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:34');
INSERT INTO `sys_log` VALUES ('109278', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:17');
INSERT INTO `sys_log` VALUES ('109279', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109280', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109281', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109282', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109283', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109284', '/logout.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:21');
INSERT INTO `sys_log` VALUES ('109285', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109286', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109287', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109288', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109289', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:24');
INSERT INTO `sys_log` VALUES ('109290', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:25');
INSERT INTO `sys_log` VALUES ('109291', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:25');
INSERT INTO `sys_log` VALUES ('109292', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:25');
INSERT INTO `sys_log` VALUES ('109293', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:27');
INSERT INTO `sys_log` VALUES ('109294', '/sysMenu!sysMenuForm.do', '?id=0', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:06');
INSERT INTO `sys_log` VALUES ('109295', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:09');
INSERT INTO `sys_log` VALUES ('109296', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:09');
INSERT INTO `sys_log` VALUES ('109297', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:11');
INSERT INTO `sys_log` VALUES ('109298', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:11');
INSERT INTO `sys_log` VALUES ('109299', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:18');
INSERT INTO `sys_log` VALUES ('109300', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:18');
INSERT INTO `sys_log` VALUES ('109301', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:25');
INSERT INTO `sys_log` VALUES ('109302', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:25');
INSERT INTO `sys_log` VALUES ('109303', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:26');
INSERT INTO `sys_log` VALUES ('109304', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:28');
INSERT INTO `sys_log` VALUES ('109305', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:28');
INSERT INTO `sys_log` VALUES ('109306', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:31');
INSERT INTO `sys_log` VALUES ('109307', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:31');
INSERT INTO `sys_log` VALUES ('109308', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:04');
INSERT INTO `sys_log` VALUES ('109309', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:04');
INSERT INTO `sys_log` VALUES ('109310', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:05');
INSERT INTO `sys_log` VALUES ('109311', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:05');
INSERT INTO `sys_log` VALUES ('109312', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:06');
INSERT INTO `sys_log` VALUES ('109313', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:06');
INSERT INTO `sys_log` VALUES ('109314', '/sysMenu!sysMenuForm.do', '?id=241', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:07');
INSERT INTO `sys_log` VALUES ('109315', '/sysMenu!sysMenuForm.do', '?id=264', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:07');
INSERT INTO `sys_log` VALUES ('109316', '/sysMenu!sysMenuForm.do', '?id=241', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:07');
INSERT INTO `sys_log` VALUES ('109317', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:08');
INSERT INTO `sys_log` VALUES ('109318', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:16');
INSERT INTO `sys_log` VALUES ('109319', '/sysMenu!addMenu.do', '?pId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:17');
INSERT INTO `sys_log` VALUES ('109320', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:17');
INSERT INTO `sys_log` VALUES ('109321', '/sysMenu!sysMenuForm.do', '?id=100027', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:17');
INSERT INTO `sys_log` VALUES ('109322', '/sysMenu!update.do', '?id=100027&icon=&title=代码生成器&state=0&actionClass=&url=sys!easyCode.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:45');
INSERT INTO `sys_log` VALUES ('109323', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:48');
INSERT INTO `sys_log` VALUES ('109324', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:48');
INSERT INTO `sys_log` VALUES ('109325', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:16:14');
INSERT INTO `sys_log` VALUES ('109326', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:16:14');
INSERT INTO `sys_log` VALUES ('109327', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 16:26:17');
INSERT INTO `sys_log` VALUES ('109328', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 16:26:17');
INSERT INTO `sys_log` VALUES ('109329', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109330', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109331', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109332', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109333', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:20');
INSERT INTO `sys_log` VALUES ('109334', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109335', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109336', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109337', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109338', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:23');
INSERT INTO `sys_log` VALUES ('109339', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:23');
INSERT INTO `sys_log` VALUES ('109340', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109341', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109342', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109343', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109344', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109345', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109346', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109347', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109348', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109349', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:26');
INSERT INTO `sys_log` VALUES ('109350', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:26');
INSERT INTO `sys_log` VALUES ('109351', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:26');
INSERT INTO `sys_log` VALUES ('109352', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:33:16');
INSERT INTO `sys_log` VALUES ('109353', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:33:16');
INSERT INTO `sys_log` VALUES ('109354', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109355', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109356', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109357', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109358', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:25');
INSERT INTO `sys_log` VALUES ('109359', '/easyCode!easyCodeForm.do', '?name=sys_log', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:29');
INSERT INTO `sys_log` VALUES ('109360', '/easyCode.do', '?name=sys_log&pKey=id&templates=javaBean-layui.ftl%40SysLog.java&templates=javaAction-layui.ftl%40SysLogAction.java&templates=javaServiceImpl-layui.ftl%40SysLogServiceImpl.java&templates=viewMain-layui.ftl%40sysLogMain.jsp&templates=viewForm-layui.ftl%40s', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:32');
INSERT INTO `sys_log` VALUES ('109361', '/easyCode.do', '?name=sys_log&pKey=id&templates=javaBean-layui.ftl%40SysLog.java&templates=javaAction-layui.ftl%40SysLogAction.java&templates=javaServiceImpl-layui.ftl%40SysLogServiceImpl.java&templates=viewMain-layui.ftl%40sysLogMain.jsp&templates=viewForm-layui.ftl%40s', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:36:11');
INSERT INTO `sys_log` VALUES ('109362', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:43');
INSERT INTO `sys_log` VALUES ('109363', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:43');
INSERT INTO `sys_log` VALUES ('109364', '/login.do', '?username=dev&password=12', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:45');
INSERT INTO `sys_log` VALUES ('109365', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109366', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109367', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109368', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109369', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:49');
INSERT INTO `sys_log` VALUES ('109370', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:49');
INSERT INTO `sys_log` VALUES ('109371', '/sysMenu!addMenu.do', '?pId=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:50');
INSERT INTO `sys_log` VALUES ('109372', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:50');
INSERT INTO `sys_log` VALUES ('109373', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:50');
INSERT INTO `sys_log` VALUES ('109374', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:51');
INSERT INTO `sys_log` VALUES ('109375', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:52');
INSERT INTO `sys_log` VALUES ('109376', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:52');
INSERT INTO `sys_log` VALUES ('109377', '/sysMenu!update.do', '?id=100068&icon=&title=系统访问日志&state=0&actionClass=&url=sysLog!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:57');
INSERT INTO `sys_log` VALUES ('109378', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:00');
INSERT INTO `sys_log` VALUES ('109379', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:01');
INSERT INTO `sys_log` VALUES ('109380', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:01');
INSERT INTO `sys_log` VALUES ('109381', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:01');
INSERT INTO `sys_log` VALUES ('109382', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:02');
INSERT INTO `sys_log` VALUES ('109383', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:25');
INSERT INTO `sys_log` VALUES ('109384', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:01');
INSERT INTO `sys_log` VALUES ('109385', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:02');
INSERT INTO `sys_log` VALUES ('109386', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:15');
INSERT INTO `sys_log` VALUES ('109387', '/sysQuartz!sysQuartzForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:16');
INSERT INTO `sys_log` VALUES ('109388', '/sysQuartz!sysQuartzForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:56');
INSERT INTO `sys_log` VALUES ('109389', '/sysQuartz!add.do', '?id=&cronexpression=0/30 * * * * ?&methodname=execute&concurrent=1&state=1&jobdetailname=logInfo&targetobject=dyl.sys.quartz.LogQuarz&triggername=系统访问日志定时器', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:52:40');
INSERT INTO `sys_log` VALUES ('109390', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:52:41');
INSERT INTO `sys_log` VALUES ('109391', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 18:16:25');
INSERT INTO `sys_log` VALUES ('109392', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 18:16:25');
INSERT INTO `sys_log` VALUES ('109393', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 18:16:26');
INSERT INTO `sys_log` VALUES ('109394', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:27');
INSERT INTO `sys_log` VALUES ('109395', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:27');
INSERT INTO `sys_log` VALUES ('109396', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:27');
INSERT INTO `sys_log` VALUES ('109397', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:28');
INSERT INTO `sys_log` VALUES ('109398', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:28');
INSERT INTO `sys_log` VALUES ('109399', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:29');
INSERT INTO `sys_log` VALUES ('109400', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:40');
INSERT INTO `sys_log` VALUES ('109401', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:44');
INSERT INTO `sys_log` VALUES ('109402', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:48');
INSERT INTO `sys_log` VALUES ('109403', '/sysQuartz!update.do', '?id=1&cronexpression=0/3 * * * * ?&methodname=haha&concurrent=1&state=1&jobdetailname=detailname&targetobject=testQuarz&triggername=测试', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:53');
INSERT INTO `sys_log` VALUES ('109404', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:53');
INSERT INTO `sys_log` VALUES ('109405', '/sysQuartz!sysQuartzForm.do', '?id=100069', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:54');
INSERT INTO `sys_log` VALUES ('109406', '/sysQuartz!update.do', '?id=100069&cronexpression=0/3 * * * * ?&methodname=execute&concurrent=1&state=0&jobdetailname=logInfo&targetobject=logQuarz&triggername=系统访问日志定时器', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:58');
INSERT INTO `sys_log` VALUES ('109407', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:59');
INSERT INTO `sys_log` VALUES ('109408', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:17:00');
INSERT INTO `sys_log` VALUES ('109409', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:01:27');
INSERT INTO `sys_log` VALUES ('109410', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109411', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109412', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109413', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109414', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:36');
INSERT INTO `sys_log` VALUES ('109415', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:36');
INSERT INTO `sys_log` VALUES ('109416', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:37');
INSERT INTO `sys_log` VALUES ('109417', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:40');
INSERT INTO `sys_log` VALUES ('109418', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:41');
INSERT INTO `sys_log` VALUES ('109419', '/sysLog!main.do', '?currentPage=4&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:53');
INSERT INTO `sys_log` VALUES ('109420', '/sysLog!main.do', '?currentPage=5&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:54');
INSERT INTO `sys_log` VALUES ('109421', '/sysLog!main.do', '?currentPage=3&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:56');
INSERT INTO `sys_log` VALUES ('109422', '/sysLog!main.do', '?currentPage=4&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:57');
INSERT INTO `sys_log` VALUES ('109423', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:03:17');
INSERT INTO `sys_log` VALUES ('109424', '/sysLog!main.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:06:25');
INSERT INTO `sys_log` VALUES ('109425', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:06:25');
INSERT INTO `sys_log` VALUES ('109426', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:06:27');
INSERT INTO `sys_log` VALUES ('109427', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:27');
INSERT INTO `sys_log` VALUES ('109428', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:27');
INSERT INTO `sys_log` VALUES ('109429', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:28');
INSERT INTO `sys_log` VALUES ('109430', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:28');
INSERT INTO `sys_log` VALUES ('109431', '/sysLog!main.do', '?currentPage=1&totalPage=26&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:30');
INSERT INTO `sys_log` VALUES ('109432', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:36');
INSERT INTO `sys_log` VALUES ('109433', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:38');
INSERT INTO `sys_log` VALUES ('109434', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:38');
INSERT INTO `sys_log` VALUES ('109435', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:39');
INSERT INTO `sys_log` VALUES ('109436', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:39');
INSERT INTO `sys_log` VALUES ('109437', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:39');
INSERT INTO `sys_log` VALUES ('109438', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109439', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109440', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109441', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109442', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109443', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109444', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109445', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109446', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109447', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:44');
INSERT INTO `sys_log` VALUES ('109448', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:45');
INSERT INTO `sys_log` VALUES ('109449', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:50');
INSERT INTO `sys_log` VALUES ('109450', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:50');
INSERT INTO `sys_log` VALUES ('109451', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:07:11');
INSERT INTO `sys_log` VALUES ('109452', '/sysLog!main.do', '?currentPage=1&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:07:13');
INSERT INTO `sys_log` VALUES ('109453', '/sysLog!main.do', '?currentPage=1&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:07:14');
INSERT INTO `sys_log` VALUES ('109454', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:07');
INSERT INTO `sys_log` VALUES ('109455', '/sysMenu!update.do', '?id=100068&icon=fa-eercast&title=系统访问日志&state=0&actionClass=&url=sysLog!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:28');
INSERT INTO `sys_log` VALUES ('109456', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:29');
INSERT INTO `sys_log` VALUES ('109457', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:29');
INSERT INTO `sys_log` VALUES ('109458', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:30');
INSERT INTO `sys_log` VALUES ('109459', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:31');
INSERT INTO `sys_log` VALUES ('109460', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:31');
INSERT INTO `sys_log` VALUES ('109461', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:31');
INSERT INTO `sys_log` VALUES ('109462', '/sysLog!main.do', '?currentPage=2&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:47');
INSERT INTO `sys_log` VALUES ('109463', '/sysLog!main.do', '?currentPage=1&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:48');
INSERT INTO `sys_log` VALUES ('109464', '/sysLog!main.do', '?currentPage=5&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:48');
INSERT INTO `sys_log` VALUES ('109465', '/sysLog!main.do', '?currentPage=4&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:49');
INSERT INTO `sys_log` VALUES ('109466', '/sysLog!main.do', '?currentPage=5&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:50');
INSERT INTO `sys_log` VALUES ('109467', '/sysLog!main.do', '?currentPage=6&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:51');
INSERT INTO `sys_log` VALUES ('109468', '/sysLog!main.do', '?currentPage=8&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:52');
INSERT INTO `sys_log` VALUES ('109469', '/sysLog!main.do', '?currentPage=10&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:52');
INSERT INTO `sys_log` VALUES ('109470', '/sysLog!main.do', '?currentPage=11&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:53');
INSERT INTO `sys_log` VALUES ('109471', '/sysLog!main.do', '?currentPage=10&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:54');
INSERT INTO `sys_log` VALUES ('109472', '/sysLog!main.do', '?currentPage=8&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:55');
INSERT INTO `sys_log` VALUES ('109473', '/sysLog!main.do', '?currentPage=7&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:55');
INSERT INTO `sys_log` VALUES ('109474', '/sysLog!main.do', '?currentPage=8&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:57');
INSERT INTO `sys_log` VALUES ('109475', '/sysLog!main.do', '?currentPage=10&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:58');
INSERT INTO `sys_log` VALUES ('109476', '/sysLog!main.do', '?currentPage=11&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:59');
INSERT INTO `sys_log` VALUES ('109477', '/sysLog!main.do', '?currentPage=12&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:59');
INSERT INTO `sys_log` VALUES ('109478', '/sysLog!main.do', '?currentPage=29&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:00');
INSERT INTO `sys_log` VALUES ('109479', '/sysLog!main.do', '?currentPage=30&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:02');
INSERT INTO `sys_log` VALUES ('109480', '/sysLog!main.do', '?currentPage=31&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:03');
INSERT INTO `sys_log` VALUES ('109481', '/sysLog!main.do', '?currentPage=29&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:05');
INSERT INTO `sys_log` VALUES ('109482', '/sysLog!main.do', '?currentPage=30&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:05');
INSERT INTO `sys_log` VALUES ('109483', '/sysLog!main.do', '?currentPage=30&totalPage=31&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:11');
INSERT INTO `sys_log` VALUES ('109484', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:21');
INSERT INTO `sys_log` VALUES ('109485', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109486', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109487', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109488', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109489', '/sysLog!main.do', '?currentPage=21&totalPage=22&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:58');
INSERT INTO `sys_log` VALUES ('109490', '/sysLog!main.do', '?currentPage=20&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:59');
INSERT INTO `sys_log` VALUES ('109491', '/sysLog!main.do', '?currentPage=18&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:59');
INSERT INTO `sys_log` VALUES ('109492', '/sysLog!main.do', '?currentPage=16&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:11:00');
INSERT INTO `sys_log` VALUES ('109493', '/sysLog!main.do', '?currentPage=14&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:11:02');
INSERT INTO `sys_log` VALUES ('109494', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:13:59');
INSERT INTO `sys_log` VALUES ('109495', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:27');
INSERT INTO `sys_log` VALUES ('109496', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:29');
INSERT INTO `sys_log` VALUES ('109497', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:41');
INSERT INTO `sys_log` VALUES ('109498', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:42');
INSERT INTO `sys_log` VALUES ('109499', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:01');
INSERT INTO `sys_log` VALUES ('109500', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:02');
INSERT INTO `sys_log` VALUES ('109501', '/sysLog!main.do', '?currentPage=1&totalPage=33&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:14');
INSERT INTO `sys_log` VALUES ('109502', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:15');
INSERT INTO `sys_log` VALUES ('109503', '/log!download.do', '?filePath=E:\\workSpace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp1\\logs\\log4j\\logInfo.log&fileName=logInfo.log&filePath=E:\\workSpace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp1\\logs\\log4j\\logInfo.log&fileName=logInfo.log', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:21');
INSERT INTO `sys_log` VALUES ('109504', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:27');
INSERT INTO `sys_log` VALUES ('109505', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:28');
INSERT INTO `sys_log` VALUES ('109506', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:28');
INSERT INTO `sys_log` VALUES ('109507', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:29');
INSERT INTO `sys_log` VALUES ('109508', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:30');
INSERT INTO `sys_log` VALUES ('109509', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:30');
INSERT INTO `sys_log` VALUES ('109510', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:30');
INSERT INTO `sys_log` VALUES ('109511', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:31');
INSERT INTO `sys_log` VALUES ('109512', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:31');
INSERT INTO `sys_log` VALUES ('109513', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:31');
INSERT INTO `sys_log` VALUES ('109514', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:32');
INSERT INTO `sys_log` VALUES ('109515', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:32');
INSERT INTO `sys_log` VALUES ('109516', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:33');
INSERT INTO `sys_log` VALUES ('109517', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:33');
INSERT INTO `sys_log` VALUES ('109518', '/sysMenu!delete.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:42');
INSERT INTO `sys_log` VALUES ('109519', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:43');
INSERT INTO `sys_log` VALUES ('109520', '/sysMenu!update.do', '?id=8&icon=fa-book&title=日志管理&state=0&actionClass=&url=sysLog!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:46');
INSERT INTO `sys_log` VALUES ('109521', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:49');
INSERT INTO `sys_log` VALUES ('109522', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:49');
INSERT INTO `sys_log` VALUES ('109523', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:49');
INSERT INTO `sys_log` VALUES ('109524', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:50');
INSERT INTO `sys_log` VALUES ('109525', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:54');
INSERT INTO `sys_log` VALUES ('109526', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:54');
INSERT INTO `sys_log` VALUES ('109527', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:55');
INSERT INTO `sys_log` VALUES ('109528', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:55');
INSERT INTO `sys_log` VALUES ('109529', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:55');
INSERT INTO `sys_log` VALUES ('109530', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:56');
INSERT INTO `sys_log` VALUES ('109531', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:56');
INSERT INTO `sys_log` VALUES ('109532', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:16:00');
INSERT INTO `sys_log` VALUES ('109533', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:16:51');
INSERT INTO `sys_log` VALUES ('109534', '/sysLog!main.do', '?currentPage=2&totalPage=35&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:16:56');
INSERT INTO `sys_log` VALUES ('109535', '/sysLog!main.do', '?currentPage=3&totalPage=35&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:17:02');
INSERT INTO `sys_log` VALUES ('109536', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:35');
INSERT INTO `sys_log` VALUES ('109537', '/sysLog!main.do', '?currentPage=2&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:40');
INSERT INTO `sys_log` VALUES ('109538', '/sysLog!main.do', '?currentPage=3&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:42');
INSERT INTO `sys_log` VALUES ('109539', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:45');
INSERT INTO `sys_log` VALUES ('109540', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:45');
INSERT INTO `sys_log` VALUES ('109541', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:46');
INSERT INTO `sys_log` VALUES ('109542', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:47');
INSERT INTO `sys_log` VALUES ('109543', '/sysLog!main.do', '?currentPage=2&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:48');
INSERT INTO `sys_log` VALUES ('109544', '/sysLog!main.do', '?currentPage=3&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:50');
INSERT INTO `sys_log` VALUES ('109545', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:10');
INSERT INTO `sys_log` VALUES ('109546', '/sysLog!main.do', '?currentPage=2&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:15');
INSERT INTO `sys_log` VALUES ('109547', '/sysLog!main.do', '?currentPage=3&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:17');
INSERT INTO `sys_log` VALUES ('109548', '/sysLog!main.do', '?currentPage=4&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:18');
INSERT INTO `sys_log` VALUES ('109549', '/sysLog!main.do', '?currentPage=5&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:35');
INSERT INTO `sys_log` VALUES ('109550', '/sysLog!main.do', '?currentPage=6&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:37');
INSERT INTO `sys_log` VALUES ('109551', '/sysLog!main.do', '?currentPage=7&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:39');
INSERT INTO `sys_log` VALUES ('109552', '/sysLog!main.do', '?currentPage=8&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:43');
INSERT INTO `sys_log` VALUES ('109553', '/sysLog!main.do', '?currentPage=9&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:45');
INSERT INTO `sys_log` VALUES ('109554', '/sysLog!main.do', '?currentPage=10&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:46');
INSERT INTO `sys_log` VALUES ('109555', '/sysLog!main.do', '?currentPage=12&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:47');
INSERT INTO `sys_log` VALUES ('109556', '/sysLog!main.do', '?currentPage=14&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:52');
INSERT INTO `sys_log` VALUES ('109557', '/sysLog!main.do', '?currentPage=16&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:54');
INSERT INTO `sys_log` VALUES ('109558', '/sysLog!main.do', '?currentPage=18&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:54');
INSERT INTO `sys_log` VALUES ('109559', '/sysLog!main.do', '?currentPage=20&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:55');
INSERT INTO `sys_log` VALUES ('109560', '/sysLog!main.do', '?currentPage=21&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:56');
INSERT INTO `sys_log` VALUES ('109561', '/sysLog!main.do', '?currentPage=23&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:57');
INSERT INTO `sys_log` VALUES ('109562', '/sysLog!main.do', '?currentPage=25&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:57');
INSERT INTO `sys_log` VALUES ('109563', '/sysLog!main.do', '?currentPage=27&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:57');
INSERT INTO `sys_log` VALUES ('109564', '/sysLog!main.do', '?currentPage=29&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:58');
INSERT INTO `sys_log` VALUES ('109565', '/sysLog!main.do', '?currentPage=31&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:58');
INSERT INTO `sys_log` VALUES ('109566', '/sysLog!main.do', '?currentPage=33&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:59');
INSERT INTO `sys_log` VALUES ('109567', '/sysLog!main.do', '?currentPage=35&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:59');
INSERT INTO `sys_log` VALUES ('109568', '/sysLog!main.do', '?currentPage=37&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:59');
INSERT INTO `sys_log` VALUES ('109569', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:08');
INSERT INTO `sys_log` VALUES ('109570', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:09');
INSERT INTO `sys_log` VALUES ('109571', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:10');
INSERT INTO `sys_log` VALUES ('109572', '/sysLog!main.do', '?currentPage=3&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:15');
INSERT INTO `sys_log` VALUES ('109573', '/sysLog!main.do', '?currentPage=4&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:16');
INSERT INTO `sys_log` VALUES ('109574', '/sysLog!main.do', '?currentPage=5&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:17');
INSERT INTO `sys_log` VALUES ('109575', '/sysLog!main.do', '?currentPage=6&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:18');
INSERT INTO `sys_log` VALUES ('109576', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:20');
INSERT INTO `sys_log` VALUES ('109577', '/sysLog!main.do', '?currentPage=4&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:23');
INSERT INTO `sys_log` VALUES ('109578', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:24');
INSERT INTO `sys_log` VALUES ('109579', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:46');
INSERT INTO `sys_log` VALUES ('109580', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:47');
INSERT INTO `sys_log` VALUES ('109581', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:14');
INSERT INTO `sys_log` VALUES ('109582', '/sysLog!main.do', '?currentPage=6&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:17');
INSERT INTO `sys_log` VALUES ('109583', '/sysLog!main.do', '?currentPage=7&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:20');
INSERT INTO `sys_log` VALUES ('109584', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:41');
INSERT INTO `sys_log` VALUES ('109585', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:47');
INSERT INTO `sys_log` VALUES ('109586', '/sysLog!main.do', '?currentPage=8&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:56');
INSERT INTO `sys_log` VALUES ('109587', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:58');
INSERT INTO `sys_log` VALUES ('109588', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:28');
INSERT INTO `sys_log` VALUES ('109589', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:28');
INSERT INTO `sys_log` VALUES ('109590', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:28');
INSERT INTO `sys_log` VALUES ('109591', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:29');
INSERT INTO `sys_log` VALUES ('109592', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 14:55:49');
INSERT INTO `sys_log` VALUES ('109593', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 14:55:58');
INSERT INTO `sys_log` VALUES ('109594', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:55:58');
INSERT INTO `sys_log` VALUES ('109595', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:55:58');
INSERT INTO `sys_log` VALUES ('109596', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:55:59');
INSERT INTO `sys_log` VALUES ('109598', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:00');
INSERT INTO `sys_log` VALUES ('109599', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:00');
INSERT INTO `sys_log` VALUES ('109600', '/sysMenu!addMenu.do', '?pId=0', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:10');
INSERT INTO `sys_log` VALUES ('109601', '/sysMenu!sysMenuForm.do', '?id=0', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:10');
INSERT INTO `sys_log` VALUES ('109602', '/sysMenu!sysMenuForm.do', '?id=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:10');
INSERT INTO `sys_log` VALUES ('109603', '/sysMenu!sysMenuForm.do', '?id=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:13');
INSERT INTO `sys_log` VALUES ('109604', '/sysMenu!update.do', '?id=109597&icon=&title=测试&state=0&actionClass=&url=&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:19');
INSERT INTO `sys_log` VALUES ('109605', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:21');
INSERT INTO `sys_log` VALUES ('109606', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:21');
INSERT INTO `sys_log` VALUES ('109607', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:21');
INSERT INTO `sys_log` VALUES ('109608', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:24');
INSERT INTO `sys_log` VALUES ('109609', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:28');
INSERT INTO `sys_log` VALUES ('109610', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:28');
INSERT INTO `sys_log` VALUES ('109611', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:31');
INSERT INTO `sys_log` VALUES ('109612', '/sysMenu!sysMenuForm.do', '?id=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:36');
INSERT INTO `sys_log` VALUES ('109613', '/sysMenu!update.do', '?id=109597&icon=&title=测试&state=0&actionClass=&url=&#xe60f;&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:56:53');
INSERT INTO `sys_log` VALUES ('109615', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:12');
INSERT INTO `sys_log` VALUES ('109616', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:12');
INSERT INTO `sys_log` VALUES ('109617', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:12');
INSERT INTO `sys_log` VALUES ('109618', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:14');
INSERT INTO `sys_log` VALUES ('109619', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:14');
INSERT INTO `sys_log` VALUES ('109620', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:15');
INSERT INTO `sys_log` VALUES ('109621', '/sysMenu!sysMenuForm.do', '?id=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:16');
INSERT INTO `sys_log` VALUES ('109622', '/sysMenu!update.do', '?id=109597&icon=&#xe60f;&title=测试&state=0&actionClass=&url=&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:21');
INSERT INTO `sys_log` VALUES ('109623', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:22');
INSERT INTO `sys_log` VALUES ('109624', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:22');
INSERT INTO `sys_log` VALUES ('109625', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:22');
INSERT INTO `sys_log` VALUES ('109626', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:26');
INSERT INTO `sys_log` VALUES ('109627', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:26');
INSERT INTO `sys_log` VALUES ('109628', '/sysMenu!addMenu.do', '?pId=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:27');
INSERT INTO `sys_log` VALUES ('109629', '/sysMenu!sysMenuForm.do', '?id=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:27');
INSERT INTO `sys_log` VALUES ('109630', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:57:27');
INSERT INTO `sys_log` VALUES ('109631', '/sysMenu!update.do', '?id=109614&icon=&#xe61b;&title=LayUi官网&state=0&actionClass=&url=http://www.layui.com/&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:58:03');
INSERT INTO `sys_log` VALUES ('109632', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:58:04');
INSERT INTO `sys_log` VALUES ('109633', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:58:04');
INSERT INTO `sys_log` VALUES ('109634', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:58:04');
INSERT INTO `sys_log` VALUES ('109635', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 14:58:10');
INSERT INTO `sys_log` VALUES ('109636', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:01:04');
INSERT INTO `sys_log` VALUES ('109637', '/easyCode!easyCodeForm.do', '?name=testtable', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:01:07');
INSERT INTO `sys_log` VALUES ('109638', '/easyCode.do', '?name=testtable&pKey=id&templates=javaBean-layui.ftl%40Testtable.java&templates=javaAction-layui.ftl%40TesttableAction.java&templates=javaServiceImpl-layui.ftl%40TesttableServiceImpl.java&templates=viewMain-layui.ftl%40testtableMain.jsp&templates=viewForm', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:01:12');
INSERT INTO `sys_log` VALUES ('109639', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:03:23');
INSERT INTO `sys_log` VALUES ('109640', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:03:23');
INSERT INTO `sys_log` VALUES ('109641', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:03:25');
INSERT INTO `sys_log` VALUES ('109642', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:25');
INSERT INTO `sys_log` VALUES ('109643', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:25');
INSERT INTO `sys_log` VALUES ('109644', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:26');
INSERT INTO `sys_log` VALUES ('109645', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:27');
INSERT INTO `sys_log` VALUES ('109646', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:27');
INSERT INTO `sys_log` VALUES ('109648', '/sysMenu!addMenu.do', '?pId=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:38');
INSERT INTO `sys_log` VALUES ('109649', '/sysMenu!sysMenuForm.do', '?id=109597', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:38');
INSERT INTO `sys_log` VALUES ('109650', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:38');
INSERT INTO `sys_log` VALUES ('109651', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:03:39');
INSERT INTO `sys_log` VALUES ('109657', '/sysMenu!update.do', '?id=109647&icon=fa-id-card-o &title=测试权限&state=0&actionClass=testtableAction&url=testtable!main.do&kind=1,2,3,4&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:17');
INSERT INTO `sys_log` VALUES ('109658', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:18');
INSERT INTO `sys_log` VALUES ('109659', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:18');
INSERT INTO `sys_log` VALUES ('109660', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:19');
INSERT INTO `sys_log` VALUES ('109661', '/testtable!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:20');
INSERT INTO `sys_log` VALUES ('109662', '/testtable!testtableForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:24');
INSERT INTO `sys_log` VALUES ('109663', '/testtable!add.do', '?id=&str2=aaa&str1=aa', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:28');
INSERT INTO `sys_log` VALUES ('109664', '/testtable!main.do', '?str2=&str1=&currentPage=1&totalPage=0', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:29');
INSERT INTO `sys_log` VALUES ('109665', '/testtable!main.do', '?str2=&str1=&currentPage=1&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:30');
INSERT INTO `sys_log` VALUES ('109666', '/testtable!testtableForm.do', '?id=109656', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:33');
INSERT INTO `sys_log` VALUES ('109667', '/testtable!update.do', '?id=109656&str2=aaa1&str1=aa', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:34');
INSERT INTO `sys_log` VALUES ('109668', '/testtable!main.do', '?str2=&str1=&currentPage=1&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:35');
INSERT INTO `sys_log` VALUES ('109669', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:39');
INSERT INTO `sys_log` VALUES ('109670', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:44');
INSERT INTO `sys_log` VALUES ('109671', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:52');
INSERT INTO `sys_log` VALUES ('109672', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:04:52');
INSERT INTO `sys_log` VALUES ('109673', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:05:08');
INSERT INTO `sys_log` VALUES ('109674', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:05:14');
INSERT INTO `sys_log` VALUES ('109675', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:05:15');
INSERT INTO `sys_log` VALUES ('109676', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:05:37');
INSERT INTO `sys_log` VALUES ('109677', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:05:38');
INSERT INTO `sys_log` VALUES ('109678', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:05:38');
INSERT INTO `sys_log` VALUES ('109679', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:05:39');
INSERT INTO `sys_log` VALUES ('109680', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:05:39');
INSERT INTO `sys_log` VALUES ('109681', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:05:39');
INSERT INTO `sys_log` VALUES ('109682', '/login.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:13');
INSERT INTO `sys_log` VALUES ('109683', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:18');
INSERT INTO `sys_log` VALUES ('109684', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:18');
INSERT INTO `sys_log` VALUES ('109685', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:25');
INSERT INTO `sys_log` VALUES ('109686', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:25');
INSERT INTO `sys_log` VALUES ('109687', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:32');
INSERT INTO `sys_log` VALUES ('109688', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:32');
INSERT INTO `sys_log` VALUES ('109689', '/login.do', '?username= test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:39');
INSERT INTO `sys_log` VALUES ('109690', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:44');
INSERT INTO `sys_log` VALUES ('109691', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:51');
INSERT INTO `sys_log` VALUES ('109692', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:51');
INSERT INTO `sys_log` VALUES ('109693', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:06:54');
INSERT INTO `sys_log` VALUES ('109694', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:07:16');
INSERT INTO `sys_log` VALUES ('109695', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:08:17');
INSERT INTO `sys_log` VALUES ('109696', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:08:25');
INSERT INTO `sys_log` VALUES ('109697', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:09:41');
INSERT INTO `sys_log` VALUES ('109698', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:10:44');
INSERT INTO `sys_log` VALUES ('109699', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:10:44');
INSERT INTO `sys_log` VALUES ('109700', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:10:44');
INSERT INTO `sys_log` VALUES ('109701', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:10:44');
INSERT INTO `sys_log` VALUES ('109702', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:10:45');
INSERT INTO `sys_log` VALUES ('109708', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:12');
INSERT INTO `sys_log` VALUES ('109709', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:13');
INSERT INTO `sys_log` VALUES ('109710', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:13');
INSERT INTO `sys_log` VALUES ('109711', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_3,2_4,109647_1&roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:26');
INSERT INTO `sys_log` VALUES ('109712', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:30');
INSERT INTO `sys_log` VALUES ('109713', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:30');
INSERT INTO `sys_log` VALUES ('109714', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:30');
INSERT INTO `sys_log` VALUES ('109715', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:32');
INSERT INTO `sys_log` VALUES ('109716', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:11:34');
INSERT INTO `sys_log` VALUES ('109717', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:34');
INSERT INTO `sys_log` VALUES ('109718', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:34');
INSERT INTO `sys_log` VALUES ('109719', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:34');
INSERT INTO `sys_log` VALUES ('109720', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:35');
INSERT INTO `sys_log` VALUES ('109721', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:36');
INSERT INTO `sys_log` VALUES ('109722', '/testtable!testtableForm.do', '?id=109656', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:41');
INSERT INTO `sys_log` VALUES ('109723', '/testtable!testtableForm.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:11:42');
INSERT INTO `sys_log` VALUES ('109724', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:20:43');
INSERT INTO `sys_log` VALUES ('109725', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:20:43');
INSERT INTO `sys_log` VALUES ('109726', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:20:46');
INSERT INTO `sys_log` VALUES ('109727', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:20:46');
INSERT INTO `sys_log` VALUES ('109728', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:20:46');
INSERT INTO `sys_log` VALUES ('109729', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:20:47');
INSERT INTO `sys_log` VALUES ('109730', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:20:48');
INSERT INTO `sys_log` VALUES ('109731', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:20:49');
INSERT INTO `sys_log` VALUES ('109739', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:21:07');
INSERT INTO `sys_log` VALUES ('109740', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:21:07');
INSERT INTO `sys_log` VALUES ('109741', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:21:08');
INSERT INTO `sys_log` VALUES ('109742', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:08');
INSERT INTO `sys_log` VALUES ('109743', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:08');
INSERT INTO `sys_log` VALUES ('109744', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:09');
INSERT INTO `sys_log` VALUES ('109745', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:10');
INSERT INTO `sys_log` VALUES ('109746', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:12');
INSERT INTO `sys_log` VALUES ('109747', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:12');
INSERT INTO `sys_log` VALUES ('109748', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:12');
INSERT INTO `sys_log` VALUES ('109749', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:13');
INSERT INTO `sys_log` VALUES ('109750', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:14');
INSERT INTO `sys_log` VALUES ('109751', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:21');
INSERT INTO `sys_log` VALUES ('109752', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:21');
INSERT INTO `sys_log` VALUES ('109753', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:25');
INSERT INTO `sys_log` VALUES ('109754', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:25');
INSERT INTO `sys_log` VALUES ('109755', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_3,2_4,109647_1,109647_2,109647_3&roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:27');
INSERT INTO `sys_log` VALUES ('109757', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:30');
INSERT INTO `sys_log` VALUES ('109758', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:21:33');
INSERT INTO `sys_log` VALUES ('109759', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:33');
INSERT INTO `sys_log` VALUES ('109760', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:33');
INSERT INTO `sys_log` VALUES ('109761', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:33');
INSERT INTO `sys_log` VALUES ('109762', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:34');
INSERT INTO `sys_log` VALUES ('109763', '/testtable!testtableForm.do', '?id=109656', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:36');
INSERT INTO `sys_log` VALUES ('109764', '/testtable!testtableForm.do', '?id=109656', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:38');
INSERT INTO `sys_log` VALUES ('109765', '/testtable!main.do', '?str2=&str1=a&currentPage=1&totalPage=1', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:42');
INSERT INTO `sys_log` VALUES ('109766', '/testtable!testtableForm.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:46');
INSERT INTO `sys_log` VALUES ('109767', '/testtable!add.do', '?id=&str2=sdfsdf&str1=sdf', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:47');
INSERT INTO `sys_log` VALUES ('109768', '/testtable!main.do', '?str2=&str1=&currentPage=1&totalPage=1', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:48');
INSERT INTO `sys_log` VALUES ('109769', '/testtable!main.do', '?str2=&str1=s&currentPage=1&totalPage=1', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:50');
INSERT INTO `sys_log` VALUES ('109770', '/testtable!main.do', '?str2=&str1=&currentPage=1&totalPage=1', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:21:53');
INSERT INTO `sys_log` VALUES ('109771', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:22:21');
INSERT INTO `sys_log` VALUES ('109772', '/easyCode!easyCodeForm.do', '?name=sys_auth_role', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:22:22');
INSERT INTO `sys_log` VALUES ('109773', '/easyCode!easyCodeForm.do', '?name=sys_auth_role', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:23:13');
INSERT INTO `sys_log` VALUES ('109774', '/easyCode!easyCodeForm.do', '?name=sys_menu', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:23:16');
INSERT INTO `sys_log` VALUES ('109775', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:39:46');
INSERT INTO `sys_log` VALUES ('109776', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:39:46');
INSERT INTO `sys_log` VALUES ('109777', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:39:48');
INSERT INTO `sys_log` VALUES ('109778', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:48');
INSERT INTO `sys_log` VALUES ('109779', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:48');
INSERT INTO `sys_log` VALUES ('109780', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:48');
INSERT INTO `sys_log` VALUES ('109781', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:50');
INSERT INTO `sys_log` VALUES ('109782', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:50');
INSERT INTO `sys_log` VALUES ('109783', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:50');
INSERT INTO `sys_log` VALUES ('109784', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:39:51');
INSERT INTO `sys_log` VALUES ('109785', '/easyCode!easyCodeForm.do', '?name=testtable', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:40:01');
INSERT INTO `sys_log` VALUES ('109786', '/easyCode.do', '?name=testtable&pKey=id&templates=javaBean-layui.ftl%40Testtable.java&templates=javaAction-layui.ftl%40TesttableAction.java&templates=javaServiceImpl-layui.ftl%40TesttableServiceImpl.java&templates=viewMain-layui.ftl%40testtableMain.jsp&templates=viewForm', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:40:05');
INSERT INTO `sys_log` VALUES ('109787', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:44:03');
INSERT INTO `sys_log` VALUES ('109788', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:44:03');
INSERT INTO `sys_log` VALUES ('109789', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:44:05');
INSERT INTO `sys_log` VALUES ('109790', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:06');
INSERT INTO `sys_log` VALUES ('109791', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:06');
INSERT INTO `sys_log` VALUES ('109792', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:06');
INSERT INTO `sys_log` VALUES ('109793', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:07');
INSERT INTO `sys_log` VALUES ('109801', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:36');
INSERT INTO `sys_log` VALUES ('109802', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:36');
INSERT INTO `sys_log` VALUES ('109803', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:36');
INSERT INTO `sys_log` VALUES ('109804', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:38');
INSERT INTO `sys_log` VALUES ('109805', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:38');
INSERT INTO `sys_log` VALUES ('109806', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:38');
INSERT INTO `sys_log` VALUES ('109807', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:41');
INSERT INTO `sys_log` VALUES ('109808', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:41');
INSERT INTO `sys_log` VALUES ('109809', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:42');
INSERT INTO `sys_log` VALUES ('109810', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:47');
INSERT INTO `sys_log` VALUES ('109811', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:48');
INSERT INTO `sys_log` VALUES ('109812', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:48');
INSERT INTO `sys_log` VALUES ('109813', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_3,2_4,109647_1,109647_2,109647_4&roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:51');
INSERT INTO `sys_log` VALUES ('109814', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:55');
INSERT INTO `sys_log` VALUES ('109815', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:44:59');
INSERT INTO `sys_log` VALUES ('109816', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:59');
INSERT INTO `sys_log` VALUES ('109817', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:59');
INSERT INTO `sys_log` VALUES ('109818', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:44:59');
INSERT INTO `sys_log` VALUES ('109819', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:45:00');
INSERT INTO `sys_log` VALUES ('109820', '/testtable!main.do', '?str2=&str1=aa&currentPage=1&totalPage=1', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:45:05');
INSERT INTO `sys_log` VALUES ('109821', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:45:55');
INSERT INTO `sys_log` VALUES ('109822', '/sysAuthKind!sysAuthKindForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:45:57');
INSERT INTO `sys_log` VALUES ('109829', '/sysAuthKind!add.do', '?id=&name=审核', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:46:05');
INSERT INTO `sys_log` VALUES ('109830', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:46:06');
INSERT INTO `sys_log` VALUES ('109831', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:46:10');
INSERT INTO `sys_log` VALUES ('109832', '/sysMenu!update.do', '?id=109647&icon=fa-id-card-o &title=测试权限&state=0&actionClass=testtableAction&url=testtable!main.do&kind=1,2,3,4,109823&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:46:16');
INSERT INTO `sys_log` VALUES ('109841', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:47:33');
INSERT INTO `sys_log` VALUES ('109842', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:47:33');
INSERT INTO `sys_log` VALUES ('109843', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:47:36');
INSERT INTO `sys_log` VALUES ('109844', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:36');
INSERT INTO `sys_log` VALUES ('109845', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:36');
INSERT INTO `sys_log` VALUES ('109846', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:36');
INSERT INTO `sys_log` VALUES ('109847', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:37');
INSERT INTO `sys_log` VALUES ('109848', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:47:42');
INSERT INTO `sys_log` VALUES ('109849', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:47:42');
INSERT INTO `sys_log` VALUES ('109850', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:47:44');
INSERT INTO `sys_log` VALUES ('109851', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:44');
INSERT INTO `sys_log` VALUES ('109852', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:44');
INSERT INTO `sys_log` VALUES ('109853', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:44');
INSERT INTO `sys_log` VALUES ('109854', '/testtable!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:45');
INSERT INTO `sys_log` VALUES ('109855', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:53');
INSERT INTO `sys_log` VALUES ('109856', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:54');
INSERT INTO `sys_log` VALUES ('109857', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:55');
INSERT INTO `sys_log` VALUES ('109858', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:56');
INSERT INTO `sys_log` VALUES ('109859', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:56');
INSERT INTO `sys_log` VALUES ('109860', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_3,2_4,109647_1,109647_2,109647_4,109647_109823&roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:47:58');
INSERT INTO `sys_log` VALUES ('109861', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:01');
INSERT INTO `sys_log` VALUES ('109862', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:48:03');
INSERT INTO `sys_log` VALUES ('109863', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:03');
INSERT INTO `sys_log` VALUES ('109864', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:03');
INSERT INTO `sys_log` VALUES ('109865', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:03');
INSERT INTO `sys_log` VALUES ('109866', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:04');
INSERT INTO `sys_log` VALUES ('109867', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:07');
INSERT INTO `sys_log` VALUES ('109868', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:07');
INSERT INTO `sys_log` VALUES ('109869', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:07');
INSERT INTO `sys_log` VALUES ('109870', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:48:08');
INSERT INTO `sys_log` VALUES ('109871', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 15:49:15');
INSERT INTO `sys_log` VALUES ('109872', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:11');
INSERT INTO `sys_log` VALUES ('109873', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:12');
INSERT INTO `sys_log` VALUES ('109874', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:13');
INSERT INTO `sys_log` VALUES ('109875', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:14');
INSERT INTO `sys_log` VALUES ('109876', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:17');
INSERT INTO `sys_log` VALUES ('109877', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:17');
INSERT INTO `sys_log` VALUES ('109878', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:19');
INSERT INTO `sys_log` VALUES ('109879', '/sysAuthKind!sysAuthKindForm.do', '?id=109823', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:21');
INSERT INTO `sys_log` VALUES ('109891', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:31');
INSERT INTO `sys_log` VALUES ('109892', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:31');
INSERT INTO `sys_log` VALUES ('109893', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_3,2_4,109647_1,109647_2,109647_4&roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:33');
INSERT INTO `sys_log` VALUES ('109894', '/sysMenu!update.do', '?id=109647&icon=fa-id-card-o &title=测试权限&state=0&actionClass=testtableAction&url=testtable!main.do&kind=1,2,3,4&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:45');
INSERT INTO `sys_log` VALUES ('109895', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:51:50');
INSERT INTO `sys_log` VALUES ('109896', '/sysAuthKind!sysAuthKindForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:58:49');
INSERT INTO `sys_log` VALUES ('109897', '/sysAuthKind!add.do', '?id=&name=sdfsdf', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:58:51');
INSERT INTO `sys_log` VALUES ('109898', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:58:52');
INSERT INTO `sys_log` VALUES ('109899', '/sysAuthKind!delete.do', '?dataIds=6', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:58:54');
INSERT INTO `sys_log` VALUES ('109906', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:02');
INSERT INTO `sys_log` VALUES ('109907', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:59:35');
INSERT INTO `sys_log` VALUES ('109908', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:59:35');
INSERT INTO `sys_log` VALUES ('109909', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 15:59:37');
INSERT INTO `sys_log` VALUES ('109910', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:37');
INSERT INTO `sys_log` VALUES ('109911', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:37');
INSERT INTO `sys_log` VALUES ('109912', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:37');
INSERT INTO `sys_log` VALUES ('109913', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:39');
INSERT INTO `sys_log` VALUES ('109914', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:39');
INSERT INTO `sys_log` VALUES ('109915', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:41');
INSERT INTO `sys_log` VALUES ('109916', '/sysMenu!update.do', '?id=109647&icon=fa-id-card-o &title=测试权限&state=0&actionClass=testtableAction&url=testtable!main.do&kind=1,2,3,4,5&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:42');
INSERT INTO `sys_log` VALUES ('109917', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:44');
INSERT INTO `sys_log` VALUES ('109918', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:46');
INSERT INTO `sys_log` VALUES ('109919', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:46');
INSERT INTO `sys_log` VALUES ('109920', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:49');
INSERT INTO `sys_log` VALUES ('109921', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:49');
INSERT INTO `sys_log` VALUES ('109922', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:52');
INSERT INTO `sys_log` VALUES ('109923', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:52');
INSERT INTO `sys_log` VALUES ('109924', '/sysRole!saveAuth.do', '?authRoleKinds=109647_5&roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 15:59:54');
INSERT INTO `sys_log` VALUES ('109925', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:00:00');
INSERT INTO `sys_log` VALUES ('109926', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:00:00');
INSERT INTO `sys_log` VALUES ('109927', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:00:05');
INSERT INTO `sys_log` VALUES ('109928', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:05');
INSERT INTO `sys_log` VALUES ('109929', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:05');
INSERT INTO `sys_log` VALUES ('109930', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:05');
INSERT INTO `sys_log` VALUES ('109931', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:06');
INSERT INTO `sys_log` VALUES ('109932', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:10');
INSERT INTO `sys_log` VALUES ('109933', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:10');
INSERT INTO `sys_log` VALUES ('109934', '/sysRole!saveAuth.do', '?authRoleKinds=&roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:12');
INSERT INTO `sys_log` VALUES ('109935', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:13');
INSERT INTO `sys_log` VALUES ('109936', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:13');
INSERT INTO `sys_log` VALUES ('109937', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:13');
INSERT INTO `sys_log` VALUES ('109938', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:17');
INSERT INTO `sys_log` VALUES ('109939', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:00:19');
INSERT INTO `sys_log` VALUES ('109940', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:19');
INSERT INTO `sys_log` VALUES ('109941', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:19');
INSERT INTO `sys_log` VALUES ('109942', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:19');
INSERT INTO `sys_log` VALUES ('109943', '/testtable!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:00:19');
INSERT INTO `sys_log` VALUES ('109944', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:28');
INSERT INTO `sys_log` VALUES ('109945', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:29');
INSERT INTO `sys_log` VALUES ('109946', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:29');
INSERT INTO `sys_log` VALUES ('109947', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:30');
INSERT INTO `sys_log` VALUES ('109948', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:32');
INSERT INTO `sys_log` VALUES ('109949', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:32');
INSERT INTO `sys_log` VALUES ('109950', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:33');
INSERT INTO `sys_log` VALUES ('109951', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:33');
INSERT INTO `sys_log` VALUES ('109952', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:37');
INSERT INTO `sys_log` VALUES ('109953', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:38');
INSERT INTO `sys_log` VALUES ('109954', '/sysAuthKind!delete.do', '?dataIds=5', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:41');
INSERT INTO `sys_log` VALUES ('109955', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:41');
INSERT INTO `sys_log` VALUES ('109956', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:44');
INSERT INTO `sys_log` VALUES ('109957', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:49');
INSERT INTO `sys_log` VALUES ('109958', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:50');
INSERT INTO `sys_log` VALUES ('109959', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:51');
INSERT INTO `sys_log` VALUES ('109960', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:51');
INSERT INTO `sys_log` VALUES ('109961', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:52');
INSERT INTO `sys_log` VALUES ('109962', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:57');
INSERT INTO `sys_log` VALUES ('109963', '/sysAuthKind!sysAuthKindForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:01:58');
INSERT INTO `sys_log` VALUES ('109964', '/sysAuthKind!add.do', '?id=&name=dfdsf', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:00');
INSERT INTO `sys_log` VALUES ('109965', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:01');
INSERT INTO `sys_log` VALUES ('109966', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:03');
INSERT INTO `sys_log` VALUES ('109967', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:03');
INSERT INTO `sys_log` VALUES ('109968', '/sysAuthKind!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:07');
INSERT INTO `sys_log` VALUES ('109969', '/sysAuthKind!delete.do', '?dataIds=5', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:09');
INSERT INTO `sys_log` VALUES ('109970', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:11');
INSERT INTO `sys_log` VALUES ('109971', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:12');
INSERT INTO `sys_log` VALUES ('109972', '/sysMenu!sysMenuForm.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:12');
INSERT INTO `sys_log` VALUES ('109973', '/sysMenu!delete.do', '?id=109647', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:30');
INSERT INTO `sys_log` VALUES ('109974', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:32');
INSERT INTO `sys_log` VALUES ('109975', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:32');
INSERT INTO `sys_log` VALUES ('109976', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:33');
INSERT INTO `sys_log` VALUES ('109977', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:02:34');
INSERT INTO `sys_log` VALUES ('109978', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:05:38');
INSERT INTO `sys_log` VALUES ('109979', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:05:38');
INSERT INTO `sys_log` VALUES ('109980', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:05:40');
INSERT INTO `sys_log` VALUES ('109981', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:40');
INSERT INTO `sys_log` VALUES ('109982', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:40');
INSERT INTO `sys_log` VALUES ('109983', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:40');
INSERT INTO `sys_log` VALUES ('109984', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:41');
INSERT INTO `sys_log` VALUES ('109985', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:41');
INSERT INTO `sys_log` VALUES ('109986', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:41');
INSERT INTO `sys_log` VALUES ('109987', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:42');
INSERT INTO `sys_log` VALUES ('109988', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:43');
INSERT INTO `sys_log` VALUES ('109989', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:43');
INSERT INTO `sys_log` VALUES ('109990', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:44');
INSERT INTO `sys_log` VALUES ('109991', '/logout.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:46');
INSERT INTO `sys_log` VALUES ('109992', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:05:48');
INSERT INTO `sys_log` VALUES ('109993', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:48');
INSERT INTO `sys_log` VALUES ('109994', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:48');
INSERT INTO `sys_log` VALUES ('109995', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:48');
INSERT INTO `sys_log` VALUES ('109996', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:49');
INSERT INTO `sys_log` VALUES ('109997', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:53');
INSERT INTO `sys_log` VALUES ('109998', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:05:57');
INSERT INTO `sys_log` VALUES ('109999', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:57');
INSERT INTO `sys_log` VALUES ('110000', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:57');
INSERT INTO `sys_log` VALUES ('110001', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:57');
INSERT INTO `sys_log` VALUES ('110002', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:58');
INSERT INTO `sys_log` VALUES ('110003', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-06-07 16:05:58');
INSERT INTO `sys_log` VALUES ('110004', '/logout.do', '', '2', '0:0:0:0:0:0:0:1', '2017-06-07 16:06:01');
INSERT INTO `sys_log` VALUES ('110005', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-07 16:06:19');
INSERT INTO `sys_log` VALUES ('110006', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:06:19');
INSERT INTO `sys_log` VALUES ('110007', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:06:19');
INSERT INTO `sys_log` VALUES ('110008', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-07 16:06:19');
INSERT INTO `sys_log` VALUES ('110009', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-07 19:16:41');
INSERT INTO `sys_log` VALUES ('110010', '/login!intologin.do', '', null, '10.5.125.131', '2017-06-07 19:17:02');
INSERT INTO `sys_log` VALUES ('110011', '/sdfsf.do', '', null, '10.5.126.115', '2017-06-07 19:28:22');
INSERT INTO `sys_log` VALUES ('110012', '/login!intologin.do', '', null, '10.5.126.115', '2017-06-07 19:28:22');
INSERT INTO `sys_log` VALUES ('110013', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-09 15:07:11');
INSERT INTO `sys_log` VALUES ('110014', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-09 15:07:13');
INSERT INTO `sys_log` VALUES ('110015', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:13');
INSERT INTO `sys_log` VALUES ('110016', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:13');
INSERT INTO `sys_log` VALUES ('110017', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:14');
INSERT INTO `sys_log` VALUES ('110018', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:14');
INSERT INTO `sys_log` VALUES ('110019', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:15');
INSERT INTO `sys_log` VALUES ('110020', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:15');
INSERT INTO `sys_log` VALUES ('110021', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:16');
INSERT INTO `sys_log` VALUES ('110022', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:19');
INSERT INTO `sys_log` VALUES ('110023', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:19');
INSERT INTO `sys_log` VALUES ('110024', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:20');
INSERT INTO `sys_log` VALUES ('110025', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:20');
INSERT INTO `sys_log` VALUES ('110026', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:21');
INSERT INTO `sys_log` VALUES ('110027', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:21');
INSERT INTO `sys_log` VALUES ('110028', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:21');
INSERT INTO `sys_log` VALUES ('110029', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:23');
INSERT INTO `sys_log` VALUES ('110030', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:31');
INSERT INTO `sys_log` VALUES ('110031', '/easyCode!main.do', '?name=&currentPage=1&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:35');
INSERT INTO `sys_log` VALUES ('110032', '/easyCode!easyCodeForm.do', '?name=sys_kind', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:36');
INSERT INTO `sys_log` VALUES ('110033', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:38');
INSERT INTO `sys_log` VALUES ('110034', '/sysLog!main.do', '?currentPage=3&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:41');
INSERT INTO `sys_log` VALUES ('110035', '/sysLog!main.do', '?currentPage=4&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:42');
INSERT INTO `sys_log` VALUES ('110036', '/sysLog!main.do', '?currentPage=5&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:43');
INSERT INTO `sys_log` VALUES ('110037', '/sysLog!main.do', '?currentPage=3&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:45');
INSERT INTO `sys_log` VALUES ('110038', '/sysLog!main.do', '?currentPage=72&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:46');
INSERT INTO `sys_log` VALUES ('110039', '/sysLog!main.do', '?currentPage=70&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:46');
INSERT INTO `sys_log` VALUES ('110040', '/sysLog!main.do', '?currentPage=69&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:47');
INSERT INTO `sys_log` VALUES ('110041', '/sysLog!main.do', '?currentPage=67&totalPage=72&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:07:47');
INSERT INTO `sys_log` VALUES ('110042', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:34');
INSERT INTO `sys_log` VALUES ('110043', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:34');
INSERT INTO `sys_log` VALUES ('110044', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:35');
INSERT INTO `sys_log` VALUES ('110045', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:35');
INSERT INTO `sys_log` VALUES ('110046', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:36');
INSERT INTO `sys_log` VALUES ('110047', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:36');
INSERT INTO `sys_log` VALUES ('110048', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:37');
INSERT INTO `sys_log` VALUES ('110049', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:37');
INSERT INTO `sys_log` VALUES ('110050', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:38');
INSERT INTO `sys_log` VALUES ('110051', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:38');
INSERT INTO `sys_log` VALUES ('110052', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:39');
INSERT INTO `sys_log` VALUES ('110053', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:39');
INSERT INTO `sys_log` VALUES ('110054', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:40');
INSERT INTO `sys_log` VALUES ('110055', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:40');
INSERT INTO `sys_log` VALUES ('110056', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:41');
INSERT INTO `sys_log` VALUES ('110057', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:41');
INSERT INTO `sys_log` VALUES ('110058', '/sysMenu!sysMenuForm.do', '?id=109614', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:42');
INSERT INTO `sys_log` VALUES ('110059', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:43');
INSERT INTO `sys_log` VALUES ('110060', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:43');
INSERT INTO `sys_log` VALUES ('110061', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:44');
INSERT INTO `sys_log` VALUES ('110062', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:44');
INSERT INTO `sys_log` VALUES ('110063', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:45');
INSERT INTO `sys_log` VALUES ('110064', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:46');
INSERT INTO `sys_log` VALUES ('110065', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:17:46');
INSERT INTO `sys_log` VALUES ('110066', '/easyCode!easyCodeForm.do', '?name=sys_auth_role', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:50:31');
INSERT INTO `sys_log` VALUES ('110067', '/easyCode.do', '?name=sys_auth_role&pKey=id&templates=javaBean-layui.ftl%40SysAuthRole.java&templates=javaAction-layui.ftl%40SysAuthRoleAction.java&templates=javaServiceImpl-layui.ftl%40SysAuthRoleServiceImpl.java&templates=viewMain-layui.ftl%40sysAuthRoleMain.jsp&templa', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:50:33');
INSERT INTO `sys_log` VALUES ('110068', '/easyCode!easyCodeForm.do', '?name=sys_auth_role', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:50:47');
INSERT INTO `sys_log` VALUES ('110069', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:51:57');
INSERT INTO `sys_log` VALUES ('110070', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:51:57');
INSERT INTO `sys_log` VALUES ('110071', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:52:06');
INSERT INTO `sys_log` VALUES ('110072', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-09 15:56:12');
INSERT INTO `sys_log` VALUES ('110073', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-09 15:56:12');
INSERT INTO `sys_log` VALUES ('110074', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-09 15:56:17');
INSERT INTO `sys_log` VALUES ('110075', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:17');
INSERT INTO `sys_log` VALUES ('110076', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:17');
INSERT INTO `sys_log` VALUES ('110077', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:17');
INSERT INTO `sys_log` VALUES ('110078', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:18');
INSERT INTO `sys_log` VALUES ('110079', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:19');
INSERT INTO `sys_log` VALUES ('110080', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:24');
INSERT INTO `sys_log` VALUES ('110081', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:24');
INSERT INTO `sys_log` VALUES ('110082', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:24');
INSERT INTO `sys_log` VALUES ('110083', '/logout.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-09 15:56:26');
INSERT INTO `sys_log` VALUES ('110084', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-12 15:24:49');
INSERT INTO `sys_log` VALUES ('110085', '/login.do', '?username=dyl&password=dyl5516273', null, '0:0:0:0:0:0:0:1', '2017-06-12 15:24:55');
INSERT INTO `sys_log` VALUES ('110086', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-12 15:25:17');
INSERT INTO `sys_log` VALUES ('110087', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-12 15:25:17');
INSERT INTO `sys_log` VALUES ('110088', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-12 15:25:17');
INSERT INTO `sys_log` VALUES ('110089', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-12 15:25:17');
INSERT INTO `sys_log` VALUES ('110090', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-12 15:25:19');
INSERT INTO `sys_log` VALUES ('110091', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-12 15:25:19');
INSERT INTO `sys_log` VALUES ('110092', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-12 15:25:23');
INSERT INTO `sys_log` VALUES ('110093', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-16 15:39:00');
INSERT INTO `sys_log` VALUES ('110094', '/login.do', '?username=dyl&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-16 15:39:03');
INSERT INTO `sys_log` VALUES ('110095', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-16 15:39:05');
INSERT INTO `sys_log` VALUES ('110096', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:05');
INSERT INTO `sys_log` VALUES ('110097', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:06');
INSERT INTO `sys_log` VALUES ('110098', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:06');
INSERT INTO `sys_log` VALUES ('110099', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:07');
INSERT INTO `sys_log` VALUES ('110100', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:07');
INSERT INTO `sys_log` VALUES ('110101', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:08');
INSERT INTO `sys_log` VALUES ('110102', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:08');
INSERT INTO `sys_log` VALUES ('110103', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:09');
INSERT INTO `sys_log` VALUES ('110104', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:09');
INSERT INTO `sys_log` VALUES ('110105', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:09');
INSERT INTO `sys_log` VALUES ('110106', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-16 15:39:23');
INSERT INTO `sys_log` VALUES ('110107', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-16 15:39:23');
INSERT INTO `sys_log` VALUES ('110108', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-16 15:39:32');
INSERT INTO `sys_log` VALUES ('110109', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:32');
INSERT INTO `sys_log` VALUES ('110110', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:32');
INSERT INTO `sys_log` VALUES ('110111', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:32');
INSERT INTO `sys_log` VALUES ('110112', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:34');
INSERT INTO `sys_log` VALUES ('110113', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:45');
INSERT INTO `sys_log` VALUES ('110114', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:39:45');
INSERT INTO `sys_log` VALUES ('110115', '/easyCode!easyCodeForm.do', '?name=sys_kind', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:15');
INSERT INTO `sys_log` VALUES ('110116', '/easyCode.do', '?name=sys_kind&pKey=id&templates=javaBean-layui.ftl%40SysKind.java&templates=javaAction-layui.ftl%40SysKindAction.java&templates=javaServiceImpl-layui.ftl%40SysKindServiceImpl.java&templates=viewMain-layui.ftl%40sysKindMain.jsp&templates=viewForm-layui.ft', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:16');
INSERT INTO `sys_log` VALUES ('110117', '/sysLog!main.do', '?currentPage=2&totalPage=77&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:31');
INSERT INTO `sys_log` VALUES ('110118', '/sysLog!main.do', '?currentPage=3&totalPage=79&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:32');
INSERT INTO `sys_log` VALUES ('110119', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:34');
INSERT INTO `sys_log` VALUES ('110120', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:46');
INSERT INTO `sys_log` VALUES ('110121', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:54');
INSERT INTO `sys_log` VALUES ('110122', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-06-16 15:41:54');
INSERT INTO `sys_log` VALUES ('110123', '/index.do', '', null, '10.5.125.131', '2017-06-16 15:42:53');
INSERT INTO `sys_log` VALUES ('110124', '/login!intologin.do', '', null, '10.5.125.131', '2017-06-16 15:42:53');
INSERT INTO `sys_log` VALUES ('110125', '/login.do', '?username=dev&password=123', null, '10.5.125.131', '2017-06-16 15:43:34');
INSERT INTO `sys_log` VALUES ('110126', '/index.do', '', '0', '10.5.125.131', '2017-06-16 15:43:34');
INSERT INTO `sys_log` VALUES ('110127', '/main.do', '', '0', '10.5.125.131', '2017-06-16 15:43:34');
INSERT INTO `sys_log` VALUES ('110128', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 15:43:34');
INSERT INTO `sys_log` VALUES ('110129', '/sysLog!main.do', '', '0', '10.5.125.131', '2017-06-16 15:43:37');
INSERT INTO `sys_log` VALUES ('110130', '/easyCode!main.do', '', '0', '10.5.125.131', '2017-06-16 15:43:38');
INSERT INTO `sys_log` VALUES ('110131', '/sysQuartz!main.do', '', '0', '10.5.125.131', '2017-06-16 15:43:38');
INSERT INTO `sys_log` VALUES ('110132', '/sysRole!main.do', '', '0', '10.5.125.131', '2017-06-16 15:43:39');
INSERT INTO `sys_log` VALUES ('110133', '/sysMenu!main.do', '', '0', '10.5.125.131', '2017-06-16 16:06:09');
INSERT INTO `sys_log` VALUES ('110134', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '10.5.125.131', '2017-06-16 16:06:09');
INSERT INTO `sys_log` VALUES ('110135', '/sysUser!main.do', '', '0', '10.5.125.131', '2017-06-16 16:06:09');
INSERT INTO `sys_log` VALUES ('110139', '/sysRole!auth.do', '?roleId=212', '0', '10.5.125.131', '2017-06-16 16:18:20');
INSERT INTO `sys_log` VALUES ('110140', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '10.5.125.131', '2017-06-16 16:18:20');
INSERT INTO `sys_log` VALUES ('110141', '/sysRole!auth.do', '?roleId=2', '0', '10.5.125.131', '2017-06-16 16:18:23');
INSERT INTO `sys_log` VALUES ('110142', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '10.5.125.131', '2017-06-16 16:18:23');
INSERT INTO `sys_log` VALUES ('110143', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_4&roleId=2', '0', '10.5.125.131', '2017-06-16 16:18:25');
INSERT INTO `sys_log` VALUES ('110144', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:18:28');
INSERT INTO `sys_log` VALUES ('110145', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-16 16:18:30');
INSERT INTO `sys_log` VALUES ('110146', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:18:30');
INSERT INTO `sys_log` VALUES ('110147', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:18:30');
INSERT INTO `sys_log` VALUES ('110148', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:18:31');
INSERT INTO `sys_log` VALUES ('110149', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:18:31');
INSERT INTO `sys_log` VALUES ('110150', '/sysUser!sysUserForm.do', '?id=2', '0', '10.5.125.131', '2017-06-16 16:21:27');
INSERT INTO `sys_log` VALUES ('110151', '/sysUser!sysUserForm.do', '?id=211', '0', '10.5.125.131', '2017-06-16 16:21:32');
INSERT INTO `sys_log` VALUES ('110152', '/sysUser!sysUserForm.do', '?id=100044', '0', '10.5.125.131', '2017-06-16 16:21:38');
INSERT INTO `sys_log` VALUES ('110153', '/sysLog!main.do', '?currentPage=1&totalPage=80&url=', '0', '10.5.125.131', '2017-06-16 16:24:07');
INSERT INTO `sys_log` VALUES ('110154', '/sysLog!main.do', '?currentPage=2&totalPage=82&url=', '0', '10.5.125.131', '2017-06-16 16:24:43');
INSERT INTO `sys_log` VALUES ('110155', '/sysLog!main.do', '?currentPage=3&totalPage=82&url=', '0', '10.5.125.131', '2017-06-16 16:24:44');
INSERT INTO `sys_log` VALUES ('110156', '/sysLog!main.do', '?currentPage=4&totalPage=82&url=', '0', '10.5.125.131', '2017-06-16 16:24:46');
INSERT INTO `sys_log` VALUES ('110157', '/sysLog!main.do', '?currentPage=5&totalPage=82&url=', '0', '10.5.125.131', '2017-06-16 16:24:47');
INSERT INTO `sys_log` VALUES ('110158', '/sysLog!main.do', '?currentPage=1&totalPage=82&url=', '0', '10.5.125.131', '2017-06-16 16:24:48');
INSERT INTO `sys_log` VALUES ('110159', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:27:32');
INSERT INTO `sys_log` VALUES ('110160', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:27:34');
INSERT INTO `sys_log` VALUES ('110161', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:27:34');
INSERT INTO `sys_log` VALUES ('110162', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:27:45');
INSERT INTO `sys_log` VALUES ('110163', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:27:45');
INSERT INTO `sys_log` VALUES ('110164', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:27:46');
INSERT INTO `sys_log` VALUES ('110165', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:27:53');
INSERT INTO `sys_log` VALUES ('110166', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:27:53');
INSERT INTO `sys_log` VALUES ('110167', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:27:53');
INSERT INTO `sys_log` VALUES ('110168', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:27:54');
INSERT INTO `sys_log` VALUES ('110169', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:27:54');
INSERT INTO `sys_log` VALUES ('110170', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:27:54');
INSERT INTO `sys_log` VALUES ('110171', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:27:58');
INSERT INTO `sys_log` VALUES ('110172', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:27:58');
INSERT INTO `sys_log` VALUES ('110173', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:27:58');
INSERT INTO `sys_log` VALUES ('110174', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:28:02');
INSERT INTO `sys_log` VALUES ('110175', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:28:02');
INSERT INTO `sys_log` VALUES ('110176', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:28:02');
INSERT INTO `sys_log` VALUES ('110177', '/sysMenu!main.do', '', '0', '10.5.125.131', '2017-06-16 16:28:08');
INSERT INTO `sys_log` VALUES ('110178', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '10.5.125.131', '2017-06-16 16:28:08');
INSERT INTO `sys_log` VALUES ('110179', '/sysUser!main.do', '', '0', '10.5.125.131', '2017-06-16 16:28:10');
INSERT INTO `sys_log` VALUES ('110180', '/sysUser!main.do', '', '0', '10.5.125.131', '2017-06-16 16:28:13');
INSERT INTO `sys_log` VALUES ('110181', '/logout.do', '', '0', '10.5.125.131', '2017-06-16 16:37:58');
INSERT INTO `sys_log` VALUES ('110182', '/login.do', '?username=dev&password=123', null, '10.5.125.131', '2017-06-16 16:39:24');
INSERT INTO `sys_log` VALUES ('110183', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:39:24');
INSERT INTO `sys_log` VALUES ('110184', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:39:24');
INSERT INTO `sys_log` VALUES ('110185', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:39:24');
INSERT INTO `sys_log` VALUES ('110186', '/sysUser!main.do', '', '0', '10.5.125.131', '2017-06-16 16:39:47');
INSERT INTO `sys_log` VALUES ('110187', '/sysMenu!main.do', '', '0', '10.5.125.131', '2017-06-16 16:39:56');
INSERT INTO `sys_log` VALUES ('110188', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '10.5.125.131', '2017-06-16 16:39:56');
INSERT INTO `sys_log` VALUES ('110190', '/sysMenu!sysMenuForm.do', '?id=2', '0', '10.5.125.131', '2017-06-16 16:40:02');
INSERT INTO `sys_log` VALUES ('110191', '/sysMenu!sysMenuForm.do', '?id=3', '0', '10.5.125.131', '2017-06-16 16:40:03');
INSERT INTO `sys_log` VALUES ('110192', '/sysMenu!sysMenuForm.do', '?id=4', '0', '10.5.125.131', '2017-06-16 16:40:04');
INSERT INTO `sys_log` VALUES ('110193', '/sysMenu!addMenu.do', '?pId=1', '0', '10.5.125.131', '2017-06-16 16:40:06');
INSERT INTO `sys_log` VALUES ('110194', '/sysMenu!sysMenuForm.do', '?id=1', '0', '10.5.125.131', '2017-06-16 16:40:06');
INSERT INTO `sys_log` VALUES ('110195', '/sysMenu!sysMenuForm.do', '?id=110189', '0', '10.5.125.131', '2017-06-16 16:40:06');
INSERT INTO `sys_log` VALUES ('110196', '/sysMenu!move.do', '?moveId=3&moveType=next&targetId=6&movePId=1&targetPId=1', '0', '10.5.125.131', '2017-06-16 16:40:08');
INSERT INTO `sys_log` VALUES ('110197', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:40:10');
INSERT INTO `sys_log` VALUES ('110198', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:40:10');
INSERT INTO `sys_log` VALUES ('110199', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:40:11');
INSERT INTO `sys_log` VALUES ('110200', '/sysQuartz!main.do', '', '0', '10.5.125.131', '2017-06-16 16:40:20');
INSERT INTO `sys_log` VALUES ('110201', '/sysMenu!main.do', '', '0', '10.5.125.131', '2017-06-16 16:40:20');
INSERT INTO `sys_log` VALUES ('110202', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '10.5.125.131', '2017-06-16 16:40:20');
INSERT INTO `sys_log` VALUES ('110203', '/sysRole!main.do', '', '0', '10.5.125.131', '2017-06-16 16:40:24');
INSERT INTO `sys_log` VALUES ('110204', '/sysRole!auth.do', '?roleId=2', '0', '10.5.125.131', '2017-06-16 16:40:44');
INSERT INTO `sys_log` VALUES ('110205', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '10.5.125.131', '2017-06-16 16:40:44');
INSERT INTO `sys_log` VALUES ('110209', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_3&roleId=2', '0', '10.5.125.131', '2017-06-16 16:41:50');
INSERT INTO `sys_log` VALUES ('110210', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:00');
INSERT INTO `sys_log` VALUES ('110211', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-16 16:42:04');
INSERT INTO `sys_log` VALUES ('110212', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:04');
INSERT INTO `sys_log` VALUES ('110213', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:04');
INSERT INTO `sys_log` VALUES ('110214', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:05');
INSERT INTO `sys_log` VALUES ('110215', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:07');
INSERT INTO `sys_log` VALUES ('110216', '/sysRole!auth.do', '?roleId=2', '0', '10.5.125.131', '2017-06-16 16:42:25');
INSERT INTO `sys_log` VALUES ('110217', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '10.5.125.131', '2017-06-16 16:42:25');
INSERT INTO `sys_log` VALUES ('110221', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_2,2_4&roleId=2', '0', '10.5.125.131', '2017-06-16 16:42:33');
INSERT INTO `sys_log` VALUES ('110222', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:42');
INSERT INTO `sys_log` VALUES ('110223', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:46');
INSERT INTO `sys_log` VALUES ('110224', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-06-16 16:42:50');
INSERT INTO `sys_log` VALUES ('110225', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:50');
INSERT INTO `sys_log` VALUES ('110226', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:50');
INSERT INTO `sys_log` VALUES ('110227', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:50');
INSERT INTO `sys_log` VALUES ('110228', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-06-16 16:42:53');
INSERT INTO `sys_log` VALUES ('110229', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '10.5.125.131', '2017-06-16 16:48:01');
INSERT INTO `sys_log` VALUES ('110230', '/sysQuartz!update.do', '?id=1&cronexpression=0/3 * * * * ?&methodname=haha&concurrent=1&state=1&jobdetailname=detailname&targetobject=testQuarz&triggername=测试', '0', '10.5.125.131', '2017-06-16 16:48:06');
INSERT INTO `sys_log` VALUES ('110231', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '10.5.125.131', '2017-06-16 16:48:07');
INSERT INTO `sys_log` VALUES ('110232', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '10.5.125.131', '2017-06-16 16:49:22');
INSERT INTO `sys_log` VALUES ('110233', '/sysQuartz!update.do', '?id=1&cronexpression=0/3 * * * * ?&methodname=haha&concurrent=1&state=0&jobdetailname=detailname&targetobject=testQuarz&triggername=测试', '0', '10.5.125.131', '2017-06-16 16:49:24');
INSERT INTO `sys_log` VALUES ('110234', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '10.5.125.131', '2017-06-16 16:49:26');
INSERT INTO `sys_log` VALUES ('110235', '/easyCode!main.do', '', '0', '10.5.125.131', '2017-06-16 16:49:28');
INSERT INTO `sys_log` VALUES ('110236', '/easyCode!easyCodeForm.do', '?name=sys_kind', '0', '10.5.125.131', '2017-06-16 16:49:43');
INSERT INTO `sys_log` VALUES ('110237', '/easyCode.do', '?name=sys_kind&pKey=id&templates=javaBean-layui.ftl%40SysKind.java&templates=javaAction-layui.ftl%40SysKindAction.java&templates=javaServiceImpl-layui.ftl%40SysKindServiceImpl.java&templates=viewMain-layui.ftl%40sysKindMain.jsp&templates=viewForm-layui.ft', '0', '10.5.125.131', '2017-06-16 16:50:11');
INSERT INTO `sys_log` VALUES ('110238', '/easyCode!main.do', '', '0', '10.5.125.131', '2017-06-16 16:51:47');
INSERT INTO `sys_log` VALUES ('110239', '/easyCode!easyCodeForm.do', '?name=t_test', '0', '10.5.125.131', '2017-06-16 16:51:50');
INSERT INTO `sys_log` VALUES ('110240', '/easyCode.do', '?name=t_test&pKey=id&templates=javaBean-layui.ftl%40Test.java&templates=javaAction-layui.ftl%40TestAction.java&templates=javaServiceImpl-layui.ftl%40TestServiceImpl.java&templates=viewMain-layui.ftl%40testMain.jsp&templates=viewForm-layui.ftl%40testForm.j', '0', '10.5.125.131', '2017-06-16 16:51:55');
INSERT INTO `sys_log` VALUES ('110241', '/index.do', '', null, '10.5.125.131', '2017-06-16 16:55:43');
INSERT INTO `sys_log` VALUES ('110242', '/login!intologin.do', '', null, '10.5.125.131', '2017-06-16 16:55:43');
INSERT INTO `sys_log` VALUES ('110243', '/login.do', '?username=dev&password=123', null, '10.5.125.131', '2017-06-16 16:55:49');
INSERT INTO `sys_log` VALUES ('110244', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:55:49');
INSERT INTO `sys_log` VALUES ('110245', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:55:49');
INSERT INTO `sys_log` VALUES ('110246', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:55:49');
INSERT INTO `sys_log` VALUES ('110247', '/sysMenu!main.do', '', '0', '10.5.125.131', '2017-06-16 16:55:51');
INSERT INTO `sys_log` VALUES ('110248', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '10.5.125.131', '2017-06-16 16:55:51');
INSERT INTO `sys_log` VALUES ('110249', '/sysMenu!sysMenuForm.do', '?id=110189', '0', '10.5.125.131', '2017-06-16 16:55:53');
INSERT INTO `sys_log` VALUES ('110251', '/sysMenu!update.do', '?id=110189&icon=&title=测试&state=0&actionClass=&url=test!main.do&note=', '0', '10.5.125.131', '2017-06-16 16:56:04');
INSERT INTO `sys_log` VALUES ('110252', '/index.do', '', '0', '10.5.125.131', '2017-06-16 16:56:05');
INSERT INTO `sys_log` VALUES ('110253', '/main.do', '', '0', '10.5.125.131', '2017-06-16 16:56:05');
INSERT INTO `sys_log` VALUES ('110254', '/sysMenu!getMenu.do', '', '0', '10.5.125.131', '2017-06-16 16:56:06');
INSERT INTO `sys_log` VALUES ('110255', '/test!main.do', '', '0', '10.5.125.131', '2017-06-16 16:56:07');
INSERT INTO `sys_log` VALUES ('110256', '/test!testForm.do', '', '0', '10.5.125.131', '2017-06-16 16:56:19');
INSERT INTO `sys_log` VALUES ('110257', '/test!add.do', '?id=&test1=1&yrdy2=2&test3=撒旦发送到仿盛大方式', '0', '10.5.125.131', '2017-06-16 16:56:24');
INSERT INTO `sys_log` VALUES ('110258', '/test!main.do', '?test1=&currentPage=1&yrdy2=&totalPage=0&test3=', '0', '10.5.125.131', '2017-06-16 16:56:25');
INSERT INTO `sys_log` VALUES ('110259', '/test!main.do', '?test1=`&currentPage=1&yrdy2=&totalPage=1&test3=', '0', '10.5.125.131', '2017-06-16 16:56:29');
INSERT INTO `sys_log` VALUES ('110261', '/test!main.do', '?test1=1&currentPage=1&yrdy2=&totalPage=0&test3=', '0', '10.5.125.131', '2017-06-16 16:56:34');
INSERT INTO `sys_log` VALUES ('110262', '/test!testForm.do', '', '0', '10.5.125.131', '2017-06-16 16:56:42');
INSERT INTO `sys_log` VALUES ('110263', '/test!add.do', '?id=&test1=sdfd&yrdy2=sdfsdf&test3=sdfssdfs', '0', '10.5.125.131', '2017-06-16 16:56:46');
INSERT INTO `sys_log` VALUES ('110264', '/test!main.do', '?test1=1&currentPage=1&yrdy2=&totalPage=1&test3=', '0', '10.5.125.131', '2017-06-16 16:56:47');
INSERT INTO `sys_log` VALUES ('110265', '/test!main.do', '?test1=&currentPage=1&yrdy2=&totalPage=1&test3=', '0', '10.5.125.131', '2017-06-16 16:56:51');
INSERT INTO `sys_log` VALUES ('110266', '/test!delete.do', '?dataIds=110260', '0', '10.5.125.131', '2017-06-16 16:56:56');
INSERT INTO `sys_log` VALUES ('110267', '/test!main.do', '?test1=&currentPage=1&yrdy2=&totalPage=1&test3=', '0', '10.5.125.131', '2017-06-16 16:56:57');
INSERT INTO `sys_log` VALUES ('110268', '/test!main.do', '?test1=&currentPage=1&yrdy2=&totalPage=1&test3=', '0', '10.5.125.131', '2017-06-16 16:56:59');
INSERT INTO `sys_log` VALUES ('110269', '/sysLog!main.do', '', '0', '10.5.125.131', '2017-06-16 16:57:41');
INSERT INTO `sys_log` VALUES ('110270', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:52');
INSERT INTO `sys_log` VALUES ('110271', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:52');
INSERT INTO `sys_log` VALUES ('110272', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:52');
INSERT INTO `sys_log` VALUES ('110273', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:52');
INSERT INTO `sys_log` VALUES ('110274', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:53');
INSERT INTO `sys_log` VALUES ('110275', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:53');
INSERT INTO `sys_log` VALUES ('110276', '/sysLog!main.do', '?currentPage=1&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 16:58:53');
INSERT INTO `sys_log` VALUES ('110277', '/log!main.do', '', '0', '10.5.125.131', '2017-06-16 17:00:13');
INSERT INTO `sys_log` VALUES ('110278', '/easyCode!main.do', '', '0', '10.5.125.131', '2017-06-16 17:00:38');
INSERT INTO `sys_log` VALUES ('110279', '/sysQuartz!main.do', '', '0', '10.5.125.131', '2017-06-16 17:00:39');
INSERT INTO `sys_log` VALUES ('110280', '/sysMenu!main.do', '', '0', '10.5.125.131', '2017-06-16 17:00:39');
INSERT INTO `sys_log` VALUES ('110281', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '10.5.125.131', '2017-06-16 17:00:39');
INSERT INTO `sys_log` VALUES ('110282', '/sysRole!main.do', '', '0', '10.5.125.131', '2017-06-16 17:00:44');
INSERT INTO `sys_log` VALUES ('110283', '/sysUser!main.do', '', '0', '10.5.125.131', '2017-06-16 17:00:45');
INSERT INTO `sys_log` VALUES ('110284', '/sysLog!main.do', '?currentPage=2&totalPage=91&url=', '0', '10.5.125.131', '2017-06-16 17:03:21');
INSERT INTO `sys_log` VALUES ('110285', '/sysLog!main.do', '?currentPage=92&totalPage=92&url=', '0', '10.5.125.131', '2017-06-16 17:03:22');
INSERT INTO `sys_log` VALUES ('110286', '/sysLog!main.do', '?currentPage=90&totalPage=92&url=', '0', '10.5.125.131', '2017-06-16 17:03:23');
INSERT INTO `sys_log` VALUES ('110287', '/sysLog!main.do', '?currentPage=91&totalPage=92&url=', '0', '10.5.125.131', '2017-06-16 17:03:24');
INSERT INTO `sys_log` VALUES ('110288', '/sysLog!main.do', '?currentPage=92&totalPage=92&url=', '0', '10.5.125.131', '2017-06-16 17:03:26');
INSERT INTO `sys_log` VALUES ('110289', '/sysLog!main.do', '?currentPage=88&totalPage=92&url=', '0', '10.5.125.131', '2017-06-16 17:03:27');
INSERT INTO `sys_log` VALUES ('110291', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-22 16:03:03');
INSERT INTO `sys_log` VALUES ('110292', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-22 16:03:05');
INSERT INTO `sys_log` VALUES ('110293', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:05');
INSERT INTO `sys_log` VALUES ('110294', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:05');
INSERT INTO `sys_log` VALUES ('110295', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:05');
INSERT INTO `sys_log` VALUES ('110296', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:07');
INSERT INTO `sys_log` VALUES ('110297', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:07');
INSERT INTO `sys_log` VALUES ('110298', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:08');
INSERT INTO `sys_log` VALUES ('110299', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:08');
INSERT INTO `sys_log` VALUES ('110300', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:09');
INSERT INTO `sys_log` VALUES ('110301', '/sysMenu!addMenu.do', '?pId=1', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:09');
INSERT INTO `sys_log` VALUES ('110302', '/sysMenu!sysMenuForm.do', '?id=110290', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:09');
INSERT INTO `sys_log` VALUES ('110303', '/sysMenu!sysMenuForm.do', '?id=110290', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:10');
INSERT INTO `sys_log` VALUES ('110304', '/sysMenu!update.do', '?id=110290&icon=fa-ellipsis-v&title=通用上传&state=0&actionClass=&url=sysFileinfo!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:38');
INSERT INTO `sys_log` VALUES ('110305', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:41');
INSERT INTO `sys_log` VALUES ('110306', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:41');
INSERT INTO `sys_log` VALUES ('110307', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:41');
INSERT INTO `sys_log` VALUES ('110308', '/sysFileinfo!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:42');
INSERT INTO `sys_log` VALUES ('110309', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:45');
INSERT INTO `sys_log` VALUES ('110310', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:45');
INSERT INTO `sys_log` VALUES ('110311', '/sysMenu!sysMenuForm.do', '?id=110290', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:03:46');
INSERT INTO `sys_log` VALUES ('110314', '/sysFileinfo!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:07:36');
INSERT INTO `sys_log` VALUES ('110315', '/upload.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:07:40');
INSERT INTO `sys_log` VALUES ('110316', '/upload.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:07:40');
INSERT INTO `sys_log` VALUES ('110317', '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=0', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:07:43');
INSERT INTO `sys_log` VALUES ('110318', '/download.do', '?fileName=201706221607464.doc&name=zhuanhuan.doc&name=zhuanhuan.doc&fileName=201706221607464.doc', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:07:44');
INSERT INTO `sys_log` VALUES ('110319', '/download.do', '?fileName=201706221607951.docx&name=zhuanhuan.docx&name=zhuanhuan.docx&fileName=201706221607951.docx', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:07:44');
INSERT INTO `sys_log` VALUES ('110321', '/upload.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:09:48');
INSERT INTO `sys_log` VALUES ('110322', '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:09:51');
INSERT INTO `sys_log` VALUES ('110323', '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:10:24');
INSERT INTO `sys_log` VALUES ('110324', '/test!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:10:55');
INSERT INTO `sys_log` VALUES ('110325', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:10:56');
INSERT INTO `sys_log` VALUES ('110326', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:10:56');
INSERT INTO `sys_log` VALUES ('110327', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:10:57');
INSERT INTO `sys_log` VALUES ('110328', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:10:57');
INSERT INTO `sys_log` VALUES ('110329', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:00');
INSERT INTO `sys_log` VALUES ('110330', '/sysMenu!sysMenuForm.do', '?id=110189', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:02');
INSERT INTO `sys_log` VALUES ('110331', '/sysMenu!delete.do', '?id=110189', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:04');
INSERT INTO `sys_log` VALUES ('110332', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:06');
INSERT INTO `sys_log` VALUES ('110333', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:06');
INSERT INTO `sys_log` VALUES ('110334', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:06');
INSERT INTO `sys_log` VALUES ('110335', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:27');
INSERT INTO `sys_log` VALUES ('110336', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:27');
INSERT INTO `sys_log` VALUES ('110337', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:27');
INSERT INTO `sys_log` VALUES ('110338', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:46');
INSERT INTO `sys_log` VALUES ('110339', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:46');
INSERT INTO `sys_log` VALUES ('110340', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:46');
INSERT INTO `sys_log` VALUES ('110341', '/sysFileinfo!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:47');
INSERT INTO `sys_log` VALUES ('110342', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-22 16:11:48');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` decimal(10,0) NOT NULL,
  `pid` decimal(10,0) DEFAULT NULL COMMENT '父Id',
  `oid` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `note` varchar(100) DEFAULT NULL COMMENT '说明',
  `state` char(1) DEFAULT '0' COMMENT '状态',
  `url` varchar(100) DEFAULT NULL COMMENT '地址',
  `icon` varchar(100) DEFAULT NULL COMMENT '图片代码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  `action_class` varchar(50) DEFAULT NULL,
  `view_level` decimal(10,0) DEFAULT '5' COMMENT '查看等级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '1', '系统管理', null, '0', null, 'fa-cubes', '2017-05-27 11:24:12', '0', null, '2');
INSERT INTO `sys_menu` VALUES ('2', '1', '-1', '用户管理', '', '0', 'sysUser!main.do', '&#xe612;', '2017-06-01 15:41:26', '0', 'sysUserAction', '2');
INSERT INTO `sys_menu` VALUES ('3', '1', '8', '菜单管理', '', '0', 'sysMenu!main.do', '&#xe62a;', '2017-06-16 16:40:08', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('4', '1', '6', '角色管理', null, '0', 'sysRole!main.do', '&#xe61b;', '2017-05-27 11:24:12', null, 'sysRoleAction', '2');
INSERT INTO `sys_menu` VALUES ('6', '1', '7', '数据库监控', '', '0', 'druid/index.html', '&#xe636;', '2017-06-01 15:16:57', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('7', '1', '9', '定时任务管理', '', '0', 'sysQuartz!main.do', '&#xe62c;', '2017-06-16 16:40:08', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('5', '1', '10', '代码生成器', '', '0', 'easyCode!main.do', '&#xe60a;', '2017-06-16 16:40:08', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('8', '1', '11', '日志管理', '', '0', 'sysLog!main.do', 'fa-book', '2017-06-16 16:40:08', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('109614', '109597', '1', 'LayUi官网', '', '0', 'http://www.layui.com/', '&#xe61b;', '2017-06-07 14:58:03', '0', '', '5');
INSERT INTO `sys_menu` VALUES ('109597', '0', '2', '测试', '', '0', '', '&#xe60f;', '2017-06-07 14:57:21', '0', '', '5');
INSERT INTO `sys_menu` VALUES ('9', '1', '13', '通用上传', '', '0', 'sysFileinfo!main.do', 'fa-ellipsis-v', '2017-06-22 16:11:44', '0', '', '1');

-- ----------------------------
-- Table structure for sys_menu_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_kind`;
CREATE TABLE `sys_menu_kind` (
  `id` decimal(10,0) NOT NULL,
  `menu_id` decimal(10,0) DEFAULT NULL COMMENT '菜单Id',
  `kind_id` decimal(10,0) DEFAULT NULL COMMENT '种类Id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_kind
-- ----------------------------
INSERT INTO `sys_menu_kind` VALUES ('206', '87', '1', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('207', '87', '2', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('192', '4', '1', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('193', '4', '2', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('100064', '2', '4', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100063', '2', '3', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100062', '2', '2', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100061', '2', '1', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('194', '4', '3', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('195', '4', '4', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('100067', '3', '2', '2017-06-01 16:34:06');
INSERT INTO `sys_menu_kind` VALUES ('100066', '3', '1', '2017-06-01 16:34:06');
INSERT INTO `sys_menu_kind` VALUES ('100057', '100053', '2', '2017-05-27 16:27:29');
INSERT INTO `sys_menu_kind` VALUES ('100056', '100053', '1', '2017-05-27 16:27:29');
INSERT INTO `sys_menu_kind` VALUES ('109903', '109647', '4', '2017-06-07 15:59:42');
INSERT INTO `sys_menu_kind` VALUES ('109902', '109647', '3', '2017-06-07 15:59:42');
INSERT INTO `sys_menu_kind` VALUES ('109901', '109647', '2', '2017-06-07 15:59:42');
INSERT INTO `sys_menu_kind` VALUES ('109900', '109647', '1', '2017-06-07 15:59:42');

-- ----------------------------
-- Table structure for sys_quartz
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz`;
CREATE TABLE `sys_quartz` (
  `id` decimal(10,0) NOT NULL,
  `triggername` varchar(40) DEFAULT NULL COMMENT '触发器名称',
  `cronexpression` varchar(40) DEFAULT NULL COMMENT '时间表达式',
  `jobdetailname` varchar(40) DEFAULT NULL COMMENT '任务名称',
  `targetobject` varchar(40) DEFAULT NULL COMMENT '目标名称',
  `methodname` varchar(40) DEFAULT NULL COMMENT '方法名称',
  `concurrent` decimal(10,0) DEFAULT '0' COMMENT '是否并发',
  `state` decimal(10,0) DEFAULT '0' COMMENT '状态',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_quartz
-- ----------------------------
INSERT INTO `sys_quartz` VALUES ('1', '测试', '0/3 * * * * ?', 'detailname', 'testQuarz', 'haha', '1', '0', '2017-06-16 16:49:24');
INSERT INTO `sys_quartz` VALUES ('100069', '系统访问日志定时器', '0/30 * * * * ?', 'logInfo', 'logQuarz', 'execute', '1', '1', '2017-06-06 10:04:53');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `note` varchar(200) DEFAULT NULL COMMENT '说明',
  `creator` decimal(10,0) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '超级管理员', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('2', '测试角色', '测试角色', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('0', '开发管理员', '开发管理员最高权限', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('212', '测试2', '测试21', '2', '2017-05-27 11:26:28');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `id` decimal(10,0) NOT NULL,
  `roleid` decimal(10,0) DEFAULT NULL COMMENT '角色id',
  `userid` decimal(10,0) DEFAULT NULL COMMENT '用户id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('209', '0', '0', '2017-05-27 11:26:47', '2');
INSERT INTO `sys_role_user` VALUES ('210', '1', '2', '2017-05-27 11:26:47', '2');
INSERT INTO `sys_role_user` VALUES ('262', '2', '211', '2017-05-27 11:26:47', '211');
INSERT INTO `sys_role_user` VALUES ('263', '212', '211', '2017-05-27 11:26:47', '211');

-- ----------------------------
-- Table structure for sys_sequence
-- ----------------------------
DROP TABLE IF EXISTS `sys_sequence`;
CREATE TABLE `sys_sequence` (
  `name` varchar(50) NOT NULL COMMENT '序列的名字，唯一',
  `current_value` bigint(20) NOT NULL COMMENT '当前的值',
  `increment_value` int(11) NOT NULL DEFAULT '1' COMMENT '步长，默认为1',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sequence
-- ----------------------------
INSERT INTO `sys_sequence` VALUES ('seq_id', '110342', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` decimal(10,0) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `state` varchar(1) DEFAULT '0' COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('0', 'dev', 'dev', '202CB962AC59075B964B07152D234B70', '大神', null, '0', '2017-05-27 11:27:09', '0');
INSERT INTO `sys_user` VALUES ('211', 'test', '测试人员1', '098F6BCD4621D373CADE4E832627B4F6', null, '222', '0', '2017-05-27 11:27:09', '0');
INSERT INTO `sys_user` VALUES ('2', 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, '0', '2017-05-27 16:32:53', '0');
INSERT INTO `sys_user` VALUES ('100044', 'test2', '测试2', 'E10ADC3949BA59ABBE56E057F20F883E', 'sdfsdf@sdf', '123123', '0', '2017-05-27 16:05:14', '0');

-- ----------------------------
-- Function structure for func_currval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_currval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_currval`(seq_name varchar(50)) RETURNS int(11)
begin
 declare value integer;
 set value = 0;
 select current_value into value
 from sys_sequence
 where name = seq_name;
 return value;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for func_nextval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_nextval`(seq_name varchar(50)) RETURNS int(11)
begin
 update sys_sequence
 set current_value = current_value + increment_value
 where name = seq_name;
 return func_currval(seq_name);
end
;;
DELIMITER ;
