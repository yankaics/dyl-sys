package dyl.sys.action;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysFileinfo;
import dyl.sys.service.SysFileinfoServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-06-06 15:31:42
 */
@Controller
public class SysFileinfoAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysFileinfoServiceImpl sysFileinfoServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@RequestMapping(value = "/sysFileinfo!main.do")
	public String main(Page page,SysFileinfo sysFileinfo,HttpServletRequest request){
		try{
			request.setAttribute("sysFileinfoList",sysFileinfoServiceImpl.findSysFileinfoList(page, sysFileinfo));
			request.setAttribute("sysFileinfo", sysFileinfo);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/sysFileinfo/sysFileinfoMain";
	}
}
