package dyl.sys.bean;

/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-06-16 16:51:55
*/

import java.io.Serializable;

public class Test implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:INTEGER(11) 主键 
	*是否可以为空:是
	*/
	
	private  Integer  id;
	
	/*字段说明：测试1
	*对应db字段名:test1 类型:VARCHAR(255)  
	*是否可以为空:是
	*/
	
	private  String  test1;
	
	/*字段说明：测试2
	*对应db字段名:yrdy2 类型:VARCHAR(255)  
	*是否可以为空:是
	*/
	
	private  String  yrdy2;
	
	/*字段说明：测试3
	*对应db字段名:test3 类型:VARCHAR(255)  
	*是否可以为空:是
	*/
	
	private  String  test3;
	
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTest1() {
        return test1;
    }
    public void setTest1(String test1) {
        this.test1 = test1;
    }
    public String getYrdy2() {
        return yrdy2;
    }
    public void setYrdy2(String yrdy2) {
        this.yrdy2 = yrdy2;
    }
    public String getTest3() {
        return test3;
    }
    public void setTest3(String test3) {
        this.test3 = test3;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"test1:"+test1+"\n"+
		
		"yrdy2:"+yrdy2+"\n"+
		
		"test3:"+test3+"\n"+
		"";
	}
}
